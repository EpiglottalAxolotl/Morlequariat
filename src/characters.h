
#ifndef __CHARACTERS__
#define __CHARACTERS__

#include <map>
#include <vector>
#include <fstream>

#include <SFML/Graphics.hpp>

#include "core.h"
#include "weapons.h"
#include "items.h" //struct item;
#include "spells.h"
#include "status.h"
#include "cutscenes.h" //easier than trying to do a forward-declaration of the cutscene event struct

#define DEFAULTOFFSET MAX_RANGE
#define WEAPONSPACE 2
#define ITEMSPACE 12
#define EQUIPSPACE 3
#define SPELLSPACE 3

#define FRAME_DURATION_DEFAULT 4

#define MAX_PARTY 3 // increase to 4 later TODO

#define ENEMY_FLY           1
#define ENEMY_INVIS         2
#define ENEMY_AVOID_SPECIAL 4
#define ENEMY_RETREAT       8
#define ENEMY_LARGE      0x10

class PlayerCharacter;

namespace characters {
	void init();
	void initEnemies();
	void startPlayerTurn();
	
	extern std::vector<PlayerCharacter*> playable;

	void setCoins(coinType, int);
	void adjustCoins(coinType, int);

	struct animation {
		int frames;
		animation* next;
		sf::IntRect* frameRects;
		sf::Vector2i* offsets;
		int frameDuration;
		bool offsetCamera; // if this is true, the camera can follow the character's pixel offset
	};
	animation* createLinearAnimation(int textureOffsetX, int textureOffsetY, int frames, int deltaX, int deltaY, int width, int height, animation* next=NULL, int durationMultiplier=FRAME_DURATION_DEFAULT, int staticOffsetX = 0, int staticOffsetY = 0);
	
	void loadNew();
	void loadSave(int fileId, int partySize);
	
	bool anyCharHasItem(int);
	void removeOneItem(int);
	
	void addNextPlayable(int x, int y);
	
	int getCoinCount(coinType);
}

class Character {
	public:
		sf::Sprite* getSprite() {return &sprite;}
		int getX() const {return x;}
		int getY() const {return y;}
		
		// The difference between the character's position on the grid and
		// the location where it should be drawn, in pixels
		int getOffsetX() const {return pixelOffsetX;}
		int getOffsetY() const {return pixelOffsetY;}
		
		void setXY(int _x, int _y){x=_x; y=_y;}
	
		virtual ~Character() = default;
	
		direction getDirection() const {return dir;}
		
		void updateAnimation();
		void setAnimation(characters::animation*, bool);
		
		bool isCameraPixelOffset() const { return currentAnimation->offsetCamera; }
		
	protected:
		int x;
		int y;
		
		sf::Sprite sprite;
		
		direction dir;
		
		int animationStart = 0;
		characters::animation* currentAnimation;
		int pixelOffsetX;
		int pixelOffsetY;
	
};

class NPC : public Character {
	public:
		NPC(int id);
		int getId() const {return id;}
		int getDialogueStart() const {return dialogueStart;}
		void setDialogueStart(int _ds) {dialogueStart = _ds;}
		void startDialogue() const;
		
		void takeSteps(direction, int);
		
		int getGiveIndex(int);
		const std::vector<cutscenes::cutsceneEvent>& getGiveResponses() const {return giveResponses;}
	
	protected:
		int id;
		int dialogueStart;
		sf::Texture tex;
		
		characters::animation* idle[4];
		characters::animation* walk[4];
		
		std::map<int, int> giveResponseIndices;
		std::vector<cutscenes::cutsceneEvent> giveResponses;
};

class StattedCharacter : public Character{
	public:
		
		void beginTurn();
		virtual void specificBeginTurn() = 0;
		
		void addCondition(conditionId,int);
		int hasStatusEffect(int) const;
		int totalStatusEffectIntensity(int) const;
		virtual void specificAddCondition(statusCondition& sc) = 0;
		
		void conditionForTurn(statusCondition& sc);
		virtual bool specificConditionForTurn(statusCondition& sc) = 0;

		void removeCondition(conditionId);
		void removeCondition(statusCondition& sc);
		virtual bool specificRemoveCondition(statusCondition& sc) = 0;
		
		virtual void moveTo(int, int, bool intermediate=false) = 0;
		
		int takeDamage(int, damage_type);
		virtual void onDeath() = 0;

		virtual void heal(int);
				
		int getDamageTaken(damage_type dt) const;
	
		int getSpeed() const {return speed;}
		int getMaxHealth() const {return hpMax;}
		int getHealth() const {return hp;}
		
		void setHealth(int health) { hp = health; }
		
		const std::vector<statusCondition>& getStatusEffects() const {return statusEffects;}
		
		void onPush(direction dir, int distance, int collisionDamage);
		virtual bool isTouchingGround() const = 0;
	
	protected:
		int hp;
		int hpMax;
		int speed;
		
		std::vector<statusCondition> statusEffects;
		
		int damageTaken[NUM_DAMAGE_TYPES] = {100, 100, 100, 100, 100, 100};
};

class PlayerCharacter : public StattedCharacter {
	public:
		PlayerCharacter(int);
		
		int getId() const {return id;}
	
		void setMoved(bool m) {_steps = m?0:speed;}
		void setActed(bool a);
		void skip(bool s) {_skip = s;}
		bool hasActed() const {return _acted || hp == 0;}
		int stepsLeft() const {return _steps;}
		bool isDone() const { return _skip || (hasActed() && stepsLeft() == 0); }
		void onDeath() override;
		
		void specificBeginTurn() override;
		void specificAddCondition(statusCondition& sc) override;
		bool specificConditionForTurn(statusCondition& sc) override;
		bool specificRemoveCondition(statusCondition& sc) override;

		void setAnimationByIndex(int);

		void heal(int) override;
		void restoreSpirit(int);
		
		std::vector<std::pair<int, int> > getWeaponTargets() const;
		
		void setDirection(direction dir);
		bool canMoveTo(int, int) const;
		void moveAction(int, int);
		void moveTo(int, int, bool intermediate=false) override;
		void attackAction(int, int, int);
		void throwItem(int, int, int);
		void useItem(int, bool immediate=false);
		void finishMoveFrom(int, int);
		
		bool takeItem(item*);
		item* popItem(int);
		int getItem(int);
		
		void equipWeapon(int);
		bool takeWeapon(weapon*);
		weapon* popWeapon(int);
		bool hasWeaponClass(weapon_class _wc) const {
			return weaponClass & (1 << _wc);
		}
		
		bool equip(int);
		void unequip(int);
		
		int getRange() const{return range;}
		
		//void castSpell(int, int);
		int inflictDamage(int, int, int, damage_type);
		
		std::string getName() const {return name;}
		
		int getMaxSpirit() const {return spMax;}
		int getSpirit() const {return sp;}
		void setSpirit(int s) {sp = s;}
		int getSpiritConsumption() const {return spiritConsumption;}
		
		bool canSpendSpirit(int) const;
		void spendSpirit(int);
		void castSpell(std::shared_ptr<spell>, int, int);
		
		int getDamageDealt(damage_type dt) const {return damageDealt[dt];}
		
		// allow for potential inventory-space-affecting effects
		const std::vector<item*> getInventory() const{return items;}
		const std::vector<weapon*> getOffhandWeapons() const{return weapons;}
		weapon* getEquippedWeapon() const {return equippedWeapon;}
		//const std::vector<int> getEquipment() const {return equipment;}
		bool hasEquipment(int) const;
		const int* getEquipment() const{ return equipment; }
		
		bool itemsFull() const { return spaceLeft == 0; }
		bool weaponsFull() const { return weaponSpaceLeft == 0; }
		bool equipsFull() const { 
		
			for (int i=0; i<EQUIPSPACE; ++i) if (!equipment[i]) return false;
			return true;
		}
		
		const std::vector<std::shared_ptr<spell>> getSpells() { return _spells; }
		
		void onArrowKey(direction);
		
		void save(int) const;
		void loadSave(int);
		
		int getRoomId() const { return roomId; }
		void setRoomId(int _roomId) { roomId = _roomId; }
		
		bool isTouchingGround() const override;
		
	private:
		int id;
	
		bool _acted;
		weapon* equippedWeapon;
		int range;
		int _steps;
		bool _skip;
		// TODO maybe a struct for a statblock, with a const one for base stats and a mutable one for actual stats
		int sp;
		int spMax;
		std::string name;
		
		int weaponClass;
		std::vector<std::shared_ptr<spell>> _spells;
		
		//item* items[ITEMSPACE];
		std::vector<item*> items;
		int spaceLeft;
		//weapon* weapons[WEAPONSPACE];
		std::vector<weapon*> weapons;
		int weaponSpaceLeft;
		int equipment[EQUIPSPACE];
	
		sf::Texture tex;
		
		int attack(int, int, int, damage_type, int);
		int weaponAttack(int, int, int);
		
		characters::animation* idle[4];
		characters::animation* walk[4];
		characters::animation* atkAnim[4];
		characters::animation* eat;
		characters::animation* ghost;
		characters::animation* sleepAnim;
		
		int damageDealt[NUM_DAMAGE_TYPES] = {100, 100, 100, 100, 50, 0};
		int spiritConsumption = 100;
		
		int roomId;
		//bool inTransit; // true means already moving and should ignore further key-press events until done
		//void completeStep(int _x, int _y);
};

#endif
