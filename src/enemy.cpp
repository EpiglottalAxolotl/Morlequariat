#include "characters.h"
#include "animations.h"
#include "world.h"
#include "triggers.h"
#include "timekeeping.h"
#include "terrain.h"
#include "enemy.h"
#include <stdlib.h> // abs
#include <iostream>
#include <fstream>
#include <sstream>
#include <utility>

using namespace std;

namespace display {
	void addAnimationAtTile(int, int, const std::shared_ptr<animations::animation>&, bool foreground=true);
}

void Enemy::specificAddCondition(statusCondition& sc) { }
bool Enemy::specificConditionForTurn(statusCondition& sc) {
	switch(sc.id) {
		case spdDn: {
			stepsLeft -= sc.intensity;
			--sc.intensity;
			if(sc.intensity == 0) removeCondition(sc);
			return true;
		}
	}
	return false;
}
bool Enemy::specificRemoveCondition(statusCondition& sc) { return false; }

struct enemyTemplate {
	int hp;
	int speed;
	int range;
	int attack;
	damage_type attackType;
	int damageTaken[NUM_DAMAGE_TYPES];
	string name;
	int flags;
	bool scriptLoaded;
	
	std::vector<BehaviorConditions> decisionTree;
	std::vector<Behavior> behaviors;
};

vector<enemyTemplate> templates;

const sf::Texture& getEnemyTexture(const int templateId) {
	static map<int, sf::Texture> enemyTextures;
	if (enemyTextures.find(templateId) == enemyTextures.end()) {
		enemyTextures.emplace(templateId, sf::Texture());
		enemyTextures[templateId].loadFromFile("img/enemy/"+to_string(templateId)+".png");
	}
	return enemyTextures[templateId];
}

std::vector<std::pair<int, int>> MovementConstraint::filterPoints(const std::vector<std::pair<int, int>>& points) const {
	vector<pair<int,int>> out;
	for(int i=0; i<points.size(); ++i) {
		if(validatePoint(points[i])) {
			out.push_back(points[i]);
		}
	}
	return out;
}

std::vector<std::pair<int, int>> MovementOptimization::filterPoints(const std::vector<std::pair<int, int>>& points) const {
	vector<pair<int,int>> out;
	int max = -65536;
	for(int i=0; i<points.size(); ++i) {
		int value = calculateMetric(points[i]);
		if(value > max) {
			out.clear();
			out.push_back(points[i]);
			max = value;
		} else if(value == max) {
			out.push_back(points[i]);
		}
	}
	return out;
}

//std::vector<std::pair<int, int>> DestinationClause::listDestinations() {
	// TODO IMPORTANT
//}

//std::vector<std::pair<int, int>> DestinationClause::filterPoints(const std::vector<std::pair<int, int>>& points) const {
	// get a list of all destinations
	// follow the path backwards until we hit a space with distance <= speed
	// (do this like a bfs, although a bit simpler: start with a list of destinations with minimal distance, create a list of all spaces with distance equal to that minus 1 that are adjacent to them,
	// then repeat but using the second list as the first list. keep going until distance == speed)
//}

class ExactMovementConstraint : public MovementConstraint {
	public:
		ExactMovementConstraint(int _x, int _y) : x(_x), y(_y) {}
	
		bool validatePoint(const std::pair<int,int>& point) const {
			return point.first == x && point.second == y;
		}
	
	private:
		int x;
		int y;
};

class LowDistancePCOptimization : public MovementOptimization {
	public:
		int calculateMetric(const std::pair<int, int>& point) const {
			int min = 65536;
			for(int i=0; i<characters::playable.size(); ++i) {
				int value = abs(characters::playable[i]->getX() - point.first) + abs(characters::playable[i]->getY() - point.second);
				if(value < min) min = value;
			}
			return -min;
		}
};

class LowSumDistanceAllPCsOptimization : public MovementOptimization {
	public:
		int calculateMetric(const std::pair<int, int>& point) const {
			int totalDistance = 0;
			for(int i=0; i<characters::playable.size(); ++i) {
				totalDistance += abs(characters::playable[i]->getX() - point.first) + abs(characters::playable[i]->getY() - point.second);
			}
			return -totalDistance;
		}
};
class HighSumDistanceAllPCsOptimization : public MovementOptimization {
	public:
		int calculateMetric(const std::pair<int, int>& point) const {
			int totalDistance = 0;
			for(int i=0; i<characters::playable.size(); ++i) {
				totalDistance += abs(characters::playable[i]->getX() - point.first) + abs(characters::playable[i]->getY() - point.second);
			}
			return totalDistance;
		}
};
class MidAvgDistanceAllPCsOptimization : public MovementOptimization {
	public:
		MidAvgDistanceAllPCsOptimization(int _optimum) : optimum(_optimum) {}

		int calculateMetric(const std::pair<int, int>& point) const {
			int totalDistance = 0;
			for(int i=0; i<characters::playable.size(); ++i) {
				totalDistance += abs(characters::playable[i]->getX() - point.first) + abs(characters::playable[i]->getY() - point.second);
			}
			totalDistance /= characters::playable.size();
			return -abs(totalDistance - optimum);
		}
	private:
		int optimum;
};


class PCsInRangeOptimization : public MovementOptimization {
	public:
		PCsInRangeOptimization(int _range) : range(_range) {}
	
		int calculateMetric(const std::pair<int, int>& point) const {
			int reachablePCs = 0;
			for(int i=0; i<characters::playable.size(); ++i) {
				if (abs(characters::playable[i]->getX() - point.first) + abs(characters::playable[i]->getY() - point.second) <= range) {
					++reachablePCs;
				}
			}
			return reachablePCs;
		}
	private:
		int range;
};

void parseScript(enemyTemplate& et, ifstream& stream) {
	/*
	1 I 94 x 6 6					use pattern 1 if there is a pc on Image 94 and the eXact space 6,6 is reachable (traversibility is lowercase, other is uppercase)
0
-1
4 0 0 l 
4 0 0 x 6 6 . l 1 d 0 15 						Speed 4, no extra flags, 0 additional prohibitions; (constraints are uppercase, optimizations are lowercase) (period separates constraints from actions) (l: lowestpc; takes 1 param for distance) (d: damage; takes param for type and param for amount) (each action is a pair of char-int*; one for target and one for effect)

*/

	std::string line;
	std::stringstream lineStream;
	
	bool conditionsDone = false;
	
	while(getline(stream, line)) {
		lineStream.str(line);
		lineStream.clear();
		
		if(!conditionsDone) {
			int behaviorIndex;
			lineStream >> behaviorIndex;
			
			if(behaviorIndex >= 0) {
				
				BehaviorConditions bc;
				bc.behaviorIndex = behaviorIndex;
				
				while(lineStream.rdbuf()->in_avail()) {
					ConditionClause condition;
					lineStream >> condition.typeId;
					
					int paramCount;
					switch(condition.typeId) {
						case 'S': // internal state is an exact number
						case 'I': // Player is standing on image
							paramCount = 1; break;
						case 'x': // Exact coordinates reachable
							paramCount = 2; break;
							
					}
					condition.params = new int[paramCount];
					for(int i=0; i<paramCount; ++i) {
						lineStream >> condition.params[i];
					}
					
					bc.conditions.push_back(condition);
				}
				
				et.decisionTree.push_back(bc);
			} else {
				conditionsDone = true;
			}
		} else {
			Behavior behavior;
			
			int temp;
			lineStream >> temp;
			behavior.staticPathfinding.speed      = temp & 0x7F;
			behavior.staticPathfinding.allowPit   = temp & 0x100;
			behavior.staticPathfinding.allowWater = temp & 0x200;
			behavior.staticPathfinding.allowLava  = temp & 0x400;
			behavior.staticPathfinding.allowWall  = temp & 0x800;
			
			if(temp & 0x80) behavior.staticPathfinding.speed *= -1;
			
			int forbidCount;
			lineStream >> forbidCount;
			for(int i=0; i<forbidCount; ++i) {
				lineStream >> temp;
				behavior.staticPathfinding.forbid.push_back(temp);
			}
			
			bool movementsDone = false;
			
			char type;
			
			while(lineStream.rdbuf()->in_avail()) {

				lineStream >> type;
				
				if(movementsDone) {
					ActionClause action;
					
					action.targetType = type;
					
					int paramCount;
					switch(type) {
						case 'l': // target lowest pc
						case 'E': // empty space in range near pc
							paramCount = 1; break;
						case 'e': // empty space as close as possible to coords
						case 'x': // exact coords
						case 'r': // relative coords
							paramCount = 2; break;
						case 'd': // nearest solid space in direction, from location
							paramCount = 3; break;
						case '=': // same target as previous
							paramCount = 0; break;
					}
					
					action.targetParams = new int[paramCount];
					for(int i=0; i<paramCount; ++i) {
						lineStream >> action.targetParams[i];
					}
					
					lineStream >> action.effectType;
					
					switch(action.effectType) {
						case 'd': // damage
							paramCount = 2; break; // damage type and amount
						case 's': // status condition
							paramCount = 2; break; // condition id and intensity
						case 'p': // push
							paramCount = 2; break; // distance and collision damage
						case 't': // transient terrain
						case '=': // set state
						case '+': // increment state
							paramCount = 1; break;
					}
					
					action.effectParams = new int[paramCount];
					for(int i=0; i<paramCount; ++i) {
						lineStream >> action.effectParams[i];
					}
					
					behavior.actions.push_back(action);
					
				} else {
					switch(type) {
						case 'X': {
							// TODO make this more generic instead of declaring adhoc param variables
							int param1, param2;
							lineStream >> param1 >> param2;
							ExactMovementConstraint* xmc = new ExactMovementConstraint(param1, param2);
							behavior.pathfinding.push_back(xmc);
							break;
						}
							
						case 'l': {
							LowDistancePCOptimization* ldpo = new LowDistancePCOptimization;
							behavior.pathfinding.push_back(ldpo);
							break;
						}
						
						case 's': {
							LowSumDistanceAllPCsOptimization* lsdapo = new LowSumDistanceAllPCsOptimization;
							behavior.pathfinding.push_back(lsdapo);
							break;
						}
						
						case 'h': {
							HighSumDistanceAllPCsOptimization* hsdapo = new HighSumDistanceAllPCsOptimization;
							behavior.pathfinding.push_back(hsdapo);
							break;
						}
						
						case 'm': {
							int param1;
							lineStream >> param1;
							MidAvgDistanceAllPCsOptimization* msdapo = new MidAvgDistanceAllPCsOptimization(param1);
							behavior.pathfinding.push_back(msdapo);
							break;
						}
						
						case 'r': {
							int param1;
							lineStream >> param1;
							PCsInRangeOptimization* piro = new PCsInRangeOptimization(param1);
							behavior.pathfinding.push_back(piro);
							break;
						}
						
						case '.': {
							movementsDone = true;
							break;
						}
					}
				}
				
			}
			
			et.behaviors.push_back(behavior);
		}
	}
}

void characters::initEnemies(){
	ifstream enemyFile("misc/enemies.asdf");
	string line;
	stringstream lineStream;
	while(getline(enemyFile, line)){
		lineStream.str(line);
		lineStream.clear();
		
		enemyTemplate et;
		
		getline(lineStream, et.name, '\t');
		
		string temp;
		getline(lineStream, temp, '\t');
		et.flags = stoi(temp);
		getline(lineStream, temp, '\t');
		et.hp = stoi(temp);
		getline(lineStream, temp, '\t');
		et.speed = stoi(temp);
		getline(lineStream, temp, '\t');
		et.range = stoi(temp);
		getline(lineStream, temp, '\t');
		et.attack = stoi(temp);
		getline(lineStream, temp, '\t');
		et.attackType = (damage_type)stoi(temp);
		
		et.scriptLoaded = false; // lazy evaluation for parsing the complex scripts
		
		for(int i=0; i<NUM_DAMAGE_TYPES; ++i){
			getline(lineStream, temp, ' ');
			et.damageTaken[i] = stoi(temp);
		}
		
		templates.push_back(et);
	}
}

Enemy::Enemy(int _x, int _y, int templateId, ItemOrWeapon* _drop){
	if(templateId >= templates.size()){
		cerr << "No such enemy " << templateId << " (tried to create at " << _x << ", " << _y << ")" << endl;
		templateId = 0;
	}
	enemyTemplate& templ = templates[templateId];
	
	if(!templ.scriptLoaded) {
		templ.scriptLoaded = true;
		cout << "misc/enemy/" + to_string(templateId) << endl;
		std::ifstream enemyScript("misc/enemy/" + to_string(templateId));
		if(enemyScript.good()) {
			parseScript(templ, enemyScript);
		}
	}
		
	
	typeId = templateId;
	
	x=_x;
	y=_y;
	spawnX = _x;
	spawnY = _y;
	hpMax = templ.hp;
	speed = templ.speed;
	range = templ.range;
	flags = templ.flags;
	attackStrength = templ.attack;
	attackType = templ.attackType;
	name = templ.name;
	state = 0;
	for(int i=0; i<NUM_DAMAGE_TYPES; ++i){
		damageTaken[i] = templ.damageTaken[i];
	}
	hp = hpMax;
	done=false;
	
	drop = _drop;

	if( flags & ENEMY_INVIS ) {
		currentAnimation = NULL;
	} else {
		const sf::Texture& tex = getEnemyTexture(templateId);
		sprite.setTexture(tex);
		int pixelSize = (flags & ENEMY_LARGE) ? 96 : 48;
		currentAnimation = characters::createLinearAnimation(0,0,tex.getSize().x/pixelSize,0,0,pixelSize,pixelSize);
	}
	
	traversibilityMap = new int** [templ.behaviors.size()];
	for(int i=0; i<templ.behaviors.size(); ++i) traversibilityMap[i] = NULL;
}

void Enemy::onDeath(){
	triggers::processGeographicTrigger(triggers::input_type::enemyDie, spawnX, spawnY, typeId);
	triggers::processGeographicTrigger(triggers::input_type::enemyExit, x, y, typeId);

	if(drop) {
		world::putItem(x, y, drop);
	}

	world::onEnemyDeath();
}

void Enemy::moveTo(int _x, int _y, bool intermediate){
	
	int oldX = x;
	int oldY = y;
	
	triggers::processPerimeterTrigger(triggers::input_type::enemyExit, x, y, _x, _y, typeId);
	
	setXY(_x, _y);
	
	triggers::processPerimeterTrigger(triggers::input_type::enemyEnter, x, y, oldX, oldY, typeId);
	
	int terrain = world::getTerrain(x,y);
	
	if(!intermediate && (terrain & (FLAG_ONEWAY | FLAG_BLOCKED)) == FLAG_ONEWAY) {
		direction pushDir = (direction) ((terrain >> 6)%4);
		Enemy* _this = this;
		timekeeping::schedule([_this, pushDir](){_this->onPush(pushDir, 1, 0);}, 2);
	}

	world::handleTransientTerrain(world::getTransientTerrain(x,y), this);

	if(terrain & FLAG_CUSTOM) {
		world::handleCustomTerrain(terrain, this);
	}
}

bool Enemy::terrainAcceptable(int x1, int y1, int x2, int y2, const StaticPathfinding& pathfinding) const {
	int terrain = world::getTerrain(x2, y2);

	for(int i=0; i<pathfinding.forbid.size(); ++i) {
		if(pathfinding.forbid[i] == terrain) return false;
	}

	if(terrain & FLAG_BLOCKED & ~FLAG_ONEWAY) return pathfinding.allowWall;
	if(world::blockedOneWay(x1, y1, x2, y2) && !pathfinding.allowWall) return false;
	// Large enemies might have bizarre interactions with one-way walls. Care about this later.

	if(terrain & FLAG_NOSTAND) {
		if(pathfinding.allowPit) return true;
		if((terrain & LIQUID_MASK) == FLAG_WATER) return pathfinding.allowWater;
		if((terrain & LIQUID_MASK) == FLAG_LAVA) return pathfinding.allowLava;
	}
	return true;
}

bool Enemy::canStep(int x1, int y1, int x2, int y2, const StaticPathfinding& pathfinding) const {
	if(world::outOfBounds(x2,y2)) return false;
	StattedCharacter* occupying = world::getCharacterAt(x2,y2);
	if(occupying && (occupying != this)) return false;

	if(!terrainAcceptable(x1, y1, x2, y2, pathfinding)) return false;

	if((flags & ENEMY_LARGE) && (
		!terrainAcceptable(x1, y1, x2 + 1, y2, pathfinding) ||
		!terrainAcceptable(x1, y1, x2, y2 + 1, pathfinding) ||
		!terrainAcceptable(x1, y1, x2 + 1, y2 + 1, pathfinding)
	)) return false;

	return true;
}

void Enemy::calculateTraversibilityMap(int behaviorIndex) {
	if(traversibilityMap[behaviorIndex]) return;
	
	int** map = new int* [world::width];
	for (int column = 0; column < world::width; ++column) {
		map[column] = new int [world::height];
		for (int row=0; row < world::height; ++row) map[column][row] = 65536;
	}
	
	// BFS to calculate effective distances
	vector<pair<pair<int,int>,int>> visited; //yes i know nested generics are bad, i'll optimize this later
	visited.push_back(make_pair(make_pair(x,y),0));
	
	std::cout << "an enemy is calculating traversibility using speed="<<stepsLeft<<std::endl;
	
	for(int i=0; i<visited.size(); ++i) {
		int tileX = visited[i].first.first;
		int tileY = visited[i].first.second;
		int distance = visited[i].second;
		
		const vector<Behavior>& behaviors = templates[typeId].behaviors;
		
		if(distance < map[tileX][tileY]) {
			map[tileX][tileY] = distance;
			if(distance <= stepsLeft + behaviors[behaviorIndex].staticPathfinding.speed) {
				if(canStep(tileX, tileY, tileX + 1, tileY, behaviors[behaviorIndex].staticPathfinding)) {
					visited.push_back(make_pair(make_pair(tileX+1,tileY),distance+1));
				}
				if(canStep(tileX, tileY, tileX - 1, tileY, behaviors[behaviorIndex].staticPathfinding)) {
					visited.push_back(make_pair(make_pair(tileX-1,tileY),distance+1));
				}
				if(canStep(tileX, tileY, tileX, tileY + 1, behaviors[behaviorIndex].staticPathfinding)) {
					visited.push_back(make_pair(make_pair(tileX,tileY+1),distance+1));
				}
				if(canStep(tileX, tileY, tileX, tileY - 1, behaviors[behaviorIndex].staticPathfinding)) {
					visited.push_back(make_pair(make_pair(tileX,tileY-1),distance+1));
				}
			}
		}
	}
	
	traversibilityMap[behaviorIndex] = map;
}

bool Enemy::isReachable(int _x, int _y, int behaviorIndex) {
	calculateTraversibilityMap(behaviorIndex); // idempotent
	
	return traversibilityMap[behaviorIndex][_x][_y] <= stepsLeft + templates[typeId].behaviors[behaviorIndex].staticPathfinding.speed;
}

bool Enemy::conditionMet(const ConditionClause& condition, int behaviorIndex) {
	switch(condition.typeId) {
		case 'I': // pc on Tile image (mostly for first dungeon boss)
			for(int i=0; i<characters::playable.size(); ++i) {
				if(world::getTileImage(characters::playable[i]->getX(), characters::playable[i]->getY()) == condition.params[0]) return true; 
			}
			return false;
			
		case 'S': // state
			return state == condition.params[0];
			
		case 'x': // can reach Exact coordinates
			return isReachable(condition.params[0], condition.params[1], behaviorIndex);
			// return params[0],params[1] is reachable using this condition's modifiers to traversability
	}
	
	return false;
}

bool Enemy::conditionsMet(const BehaviorConditions& conditions) {
	for(int i=0; i<conditions.conditions.size(); ++i) if (!conditionMet(conditions.conditions[i], conditions.behaviorIndex)) return false;
	return true;
}

void Enemy::chooseDestination(int behaviorIndex) {
	const Behavior& behavior = templates[typeId].behaviors[behaviorIndex];
	
	calculateTraversibilityMap(behaviorIndex); // idempotent; if the map is already calculated it does nothing
	
	std::vector<std::pair<int, int>> candidateDestinations;
	for(int i=0; i<world::width; ++i) {
		for(int j=0; j<world::height; ++j) {
			if(isReachable(i, j, behaviorIndex)) {
				candidateDestinations.push_back(make_pair(i,j));
			}
		}
	}
	
	cout << "There are " << candidateDestinations.size() << " candidate destinations" << std::endl;
	if (candidateDestinations.size() == 0) {
		// literally impossible to move
		return;
	}
	
	for(int i=0; i<behavior.pathfinding.size(); ++i) {
		vector<pair<int,int>> newDestinations = behavior.pathfinding[i]->filterPoints(candidateDestinations);
		if(newDestinations.size() > 0) candidateDestinations = newDestinations;
		if(candidateDestinations.size() == 1) break;
	}
	
	path.push_back(candidateDestinations[0]); // just do it arbitrarily, if we care about tiebreaking it'll be in the pathfinding constraints
	
	int destinationX = candidateDestinations[0].first;
	int destinationY = candidateDestinations[0].second;

	int distance = traversibilityMap[behaviorIndex][destinationX][destinationY] - 1;
	while(distance > 0) {
		if(destinationX > 0 && traversibilityMap[behaviorIndex][destinationX-1][destinationY] == distance) {
			--destinationX;
		} else if(destinationX < world::width-1 && traversibilityMap[behaviorIndex][destinationX+1][destinationY] == distance) {
			++destinationX;
		} else if(destinationY > 0 && traversibilityMap[behaviorIndex][destinationX][destinationY-1] == distance) {
			--destinationY;
		} else if(destinationY < world::height-1 && traversibilityMap[behaviorIndex][destinationX][destinationY+1] == distance) {
			++destinationY;
		} else {
			// Fatal error: traversibility map in inconsistent state
		}
		
		--distance;
		path.push_back(make_pair(destinationX,destinationY));
	}
	
	cout << "Currently at " << x << ", " << y << endl;
	for (int i=path.size()-1; i>=0; --i) {
		cout << "path will step to " << path[i].first << ", " << path[i].second << " which has distance " << traversibilityMap[behaviorIndex][path[i].first][path[i].second] << endl;
	}
}

// returns how many frames until the animation is done
int Enemy::damageLocation(int x, int y, int amt, damage_type dtype) {
	StattedCharacter* target = world::getCharacterAt(x,y);
	int deltax = x - this->x;
	int deltay = y - this->y;
	
	if (flags & ENEMY_LARGE) {
		if(x > this->x) --deltax;
		if(y > this->y) --deltay;
	}

	if(deltax * deltax + deltay * deltay > 1) {
		damage_type attackType = this->attackType;
		Enemy* _this = this;
		
		int sourceX = this->x;
		int sourceY = this->y;
		
		if (flags & ENEMY_LARGE) {
			if (x > sourceX) sourceX += 1;
			if (y > sourceY) sourceY += 1;
		}

		int projectileFrames = animations::animateProjectile(sourceX, sourceY, x, y, attackType, [target, amt, attackType, x, y, _this](){
			display::addAnimationAtTile(x,y, animations::takeDamage[attackType] );
			if(target){
				target->takeDamage(amt, attackType);
			} else {
				world::damageTerrain(x, y, amt, attackType);			
			}
		});

		return animations::takeDamage[dtype]->frames + projectileFrames;
	} else {
		display::addAnimationAtTile(x,y, animations::takeDamage[attackType] );
		if(target){
			target->takeDamage(amt, attackType);
		} else {
			world::damageTerrain(x, y, amt, attackType);
		}
		return animations::takeDamage[dtype]->frames;
	}
}

int Enemy::takeAction(int behaviorIndex) {
	
	int totalFrames = 0;

	int targetX = -1;
	int targetY = -1;

	for(int i=0; i<templates[typeId].behaviors[behaviorIndex].actions.size(); ++i) {
		
		const ActionClause& action = templates[typeId].behaviors[behaviorIndex].actions[i];
		
		switch(action.targetType) {
			case '=': break; // Use the same target coords as before
			
			case 'x': {
				targetX = action.targetParams[0];
				targetY = action.targetParams[1];
				break;
			}
			case 'l': {
				int mindex = -1;
				for(int i=0; i<characters::playable.size(); ++i) {
					PlayerCharacter* pc = characters::playable[i];
					if ( std::abs(x - pc->getX()) + std::abs(y - pc->getY()) <= action.targetParams[0]
					&& (mindex < 0 || characters::playable[i]->getHealth() < characters::playable[mindex]->getHealth())) {
						mindex = i;
					}
				}
				if (mindex >= 0) {
					targetX = characters::playable[mindex]->getX();
					targetY = characters::playable[mindex]->getY();
				} else {
					targetX = -1;
					targetY = -1;
				}
				break;
			}
			case 'E': {
				int minDistance = 65536;
				for(int i = -action.targetParams[0]; i < action.targetParams[0]; ++i) {
					for(int j = -action.targetParams[0]; j < action.targetParams[0]; ++j) {
						if (!world::blockedOrOccupied(x+i,y+j)) {
							for(int k=0; k<characters::playable.size(); ++k) {
								PlayerCharacter* pc = characters::playable[k];
								int distance = std::abs(x + i - pc->getX()) + std::abs(y + j - pc->getY());
								if (distance < minDistance) {
									minDistance = distance;
									targetX = x + i;
									targetY = y + j;
								}
							}
						}
					}
				}
				
				break;
			}
			case 'e': {
				targetX = action.targetParams[0];
				targetY = action.targetParams[1];
				int length = 1;
				int delta = 0;
				direction dir = _up;
				while(blockedOrOccupied(targetX, targetY)) {
					switch(dir) {
						case _up: --targetY; break;
						case _left: --targetX; break;
						case _down: ++targetY; break;
						case _right: ++targetX; break;
					}
					++delta;
					if(delta == length) {
						delta = 0;
						switch(dir) {
							case _up: dir = _left; break;
							case _left: dir = _down; ++length; break;
							case _down: dir = _right; break;
							case _right: dir = _up; ++length; break;
						}
					}
				}
				break;
			}
			
			case 'd': { // nearest solid space in a direction
				int deltaX = 0;
				int deltaY = 0;
				switch(action.targetParams[0]) {
					case 0:
						deltaX = -1;
						break;
					case 1:
						deltaY = -1;
						break;
					case 2:
						deltaX = 1;
						break;
					case 3:
						deltaY = 1;
						break;
					case 4:
						deltaX = -1;
						deltaY = -1;
						break;
					case 5:
						deltaX = 1;
						deltaY = -1;
						break;
					case 6:
						deltaX = 1;
						deltaY = 1;
						break;
					case 7:
						deltaX = -1;
						deltaY = 1;
						break;
				}
				targetX = action.targetParams[1] + x;
				targetY = action.targetParams[2] + y;
				while (!blockedOrOccupied(targetX, targetY)) {
					targetX += deltaX;
					targetY += deltaY;
				}
				break;
			}
			
			case 'r': { //relative position
				targetX = action.targetParams[0] + x;
				targetY = action.targetParams[1] + y;
				break;
			}
		}
		
		if(targetX < 0 || targetY < 0) continue;
		
		int thisFrames;
		
		switch(action.effectType) {
			case 'd': {
				thisFrames = damageLocation(targetX, targetY, action.effectParams[1], (damage_type) action.effectParams[0]);
				break;
			}
			case 's': { // spawn
				if (!world::getCharacterAt(targetX, targetY)) {
					
					int deltax = this->x - x;
					int deltay = this->y - y;
					int spawnId = action.effectParams[0];
					
					thisFrames = animations::animateProjectile(this->x, this->y, targetX, targetY, attackType, [targetX, targetY, spawnId](){
						Enemy* spawned = world::putEnemy(targetX, targetY, spawnId, NULL);
						spawned->setDone(); // spawned minions should wait
					});
				}
				break;
			}
			case 'p': { // push away
				StattedCharacter* target = world::getCharacterAt(targetX, targetY);
				if(target) {
					if(target->getX() == this->x) {
						if(this->y > target->getY()) {
							target->onPush(_up, action.effectParams[0], action.effectParams[1]);
						} else {
							target->onPush(_down, action.effectParams[0], action.effectParams[1]);
						}
					} else {
						if(this->x > target->getX()) {
							target->onPush(_left, action.effectParams[0], action.effectParams[1]);
						} else {
							target->onPush(_right, action.effectParams[0], action.effectParams[1]);
						}
					}
					thisFrames = action.effectParams[0];
				}
				
				break;
			
			}
			case 't': { // transient terrain
				world::setTransientTerrain(targetX, targetY, action.effectParams[0]);
				thisFrames = 0; // TODO in the future this should have a projectile associated!
				break;
			}
			case '=': {
				state = action.effectParams[0];
				thisFrames = 0;
				break;
			}
			case '+': {
				state += action.effectParams[0];
				thisFrames = 0;
				break;
			}
		}
		
		if(thisFrames > totalFrames) totalFrames = thisFrames;
	}
	
	return totalFrames;
}

void Enemy::takePathStep(int behaviorIndex) {
	
	if(path.size() == 0) {
		int delay = takeAction(behaviorIndex);
		if (delay == 0) setDone();
		else timekeeping::schedule([this](){this->setDone();}, delay);

	} else {	
		pair<int,int> destination = path.back();
		path.pop_back();
		cout << "Moving to " << destination.first << ", " << destination.second << endl;
		moveTo(destination.first, destination.second);
		int frameDelay = currentAnimation ? currentAnimation->frames : 4;
		if(hp > 0) timekeeping::schedule([this, behaviorIndex](){this->takePathStep(behaviorIndex);}, frameDelay);
		
	}
}

bool Enemy::blockedOrOccupied(int x, int y) const {
	if(world::blocked(x, y)) return true;
	StattedCharacter* occupyingCharacter = world::getCharacterAt(x,y);
	return occupyingCharacter && (occupyingCharacter != this);
}

void Enemy::takeStep(){
	if(hp < 0) return;
	
	int minDistance = 65536; // TODO get a better arbitrarily large number
	//PlayerCharacter* closestChar = NULL;
	int targetX = 0;
	int targetY = 0;
	
	// Find the closest character
	for(int i=0; i < characters::playable.size(); ++i){
		PlayerCharacter* pc = characters::playable[i];
		if (pc->getHealth() == 0) continue;
		
		int distance = std::abs(x - pc->getX()) + std::abs(y - pc->getY());
		if (flags & ENEMY_LARGE) {
			if(pc->getX() > x) --distance;
			if(pc->getY() > y) --distance;
		}
		if (distance < minDistance) {
			minDistance = distance;
			//closestChar = pc;
			targetX = pc->getX();
			targetY = pc->getY();
			
		}
	}
	
	if(!hasAttacked && (minDistance == range || (minDistance < range && stepsLeft == 0))) {
		int attackDelay = damageLocation(targetX, targetY, attackStrength, attackType);
		hasAttacked = true;
		int frameDelay = currentAnimation ? currentAnimation->frames : 4;
		if (flags & ENEMY_RETREAT) timekeeping::schedule([this](){this->takeStep();}, frameDelay);
		else timekeeping::schedule([this](){this->setDone();}, attackDelay);
	} else if(stepsLeft > 0) {
		int deltaX = x - targetX;
		int deltaY = y - targetY;
		
		bool retreat = (minDistance < range) || (hasAttacked && (flags & ENEMY_RETREAT));
		
		std::vector<std::pair<int, int>> options;

		std::pair<int, int> xStep;
		std::pair<int, int> yStep;
		
		if((deltaX > 0 && !retreat) || (deltaX < 0 && retreat)){
			xStep = std::make_pair(x-1, y);
		} else {
			xStep = std::make_pair(x+1, y);
		}
		
		if((deltaY > 0 && !retreat) || (deltaY < 0 && retreat)){
			yStep = std::make_pair(x, y-1);
		} else {
			yStep = std::make_pair(x, y+1);
		}
				
		if(std::abs(deltaX) > std::abs(deltaY)){
			options.push_back(xStep);
			if(deltaY != 0) options.push_back(yStep);
		} else {
			options.push_back(yStep);
			if(deltaX != 0) options.push_back(xStep);
		}
		
		if(retreat) {
			if(deltaX == 0) {
				options.push_back(std::make_pair(x+1, y));
				options.push_back(std::make_pair(x-1, y));
			}
			if(deltaY == 0) {
				options.push_back(std::make_pair(x, y+1));
				options.push_back(std::make_pair(x, y-1));
			}
		}
		
		for(int i=0; i<options.size(); ++i) {
			int terrain = world::getTerrain(options[i].first, options[i].second);
			if( !(
				blockedOrOccupied(options[i].first, options[i].second) || // can't move there if it's blocked or occupied
				((terrain & FLAG_NOSTAND) && !(flags & ENEMY_FLY)) || // can't move there if there's a pit and it doesn't fly
				((flags & ENEMY_AVOID_SPECIAL) && (terrain & FLAG_CUSTOM)) || // can't move there if there's a custom effect it doesn't like
				world::blockedOneWay(x, y, options[i].first, options[i].second) || // can't move up a cliff
				((flags & ENEMY_LARGE) && (
					blockedOrOccupied(options[i].first + 1, options[i].second) ||
					blockedOrOccupied(options[i].first, options[i].second + 1) ||
					blockedOrOccupied(options[i].first + 1, options[i].second + 1)
				))
			)){
				moveTo(options[i].first, options[i].second);
				break;
			}
		}
		// TODO if can't move away, attack...?
		
		--stepsLeft;
    
    
		int frameDelay = currentAnimation ? currentAnimation->frames : 4;
		if(hp > 0) timekeeping::schedule([this](){this->takeStep();}, frameDelay);
	} else done = true;
}

void Enemy::clearMovementCache() {
	path.clear();
	
	for(int i=0; i<templates[typeId].behaviors.size(); ++i) {
		if(traversibilityMap[i]) {
			for (int j = 0; j < world::width; ++j) {
				delete[] traversibilityMap[i][j];
			}
			delete[] traversibilityMap[i];
			traversibilityMap[i] = NULL;
		}
	}
}

void Enemy::specificBeginTurn(){
	
	stepsLeft = speed;
	hasAttacked = false;
	done = false;
	
	clearMovementCache(); // removes cached path target and traversibility maps
	
	beginTurn();
	
	const vector<BehaviorConditions>& decisionTree = templates[typeId].decisionTree;
	
	int behaviorIndex = -1;
	for(int i=0; i<decisionTree.size(); ++i) {
		if(conditionsMet(decisionTree[i])) {
			behaviorIndex = decisionTree[i].behaviorIndex;
			break;
		}
	}
	
	if(behaviorIndex >= 0) {
		stepsLeft += templates[typeId].behaviors[behaviorIndex].staticPathfinding.speed;
		chooseDestination(behaviorIndex);
		takePathStep(behaviorIndex);
	} else {
		takeStep(); // use old pathfinding
	}
	
}

bool Enemy::isTouchingGround() const {
	return ! (flags & ENEMY_FLY);
}
