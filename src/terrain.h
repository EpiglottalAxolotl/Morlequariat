#ifndef TERRAIN_H
#define TERRAIN_H

#define FLAG_BLOCKED 0x01
#define FLAG_NOSTAND 0x02
#define FLAG_WATER   0x04
#define FLAG_LAVA    0x08
#define FLAG_BREAK   0x10
#define FLAG_ONEWAY  0x20
#define FLAG_CUSTOM  0x40
#define FLAG_CUSTOM2 0x80
//#define FLAG_DOOR    0x80
#define LIQUID_MASK  (FLAG_WATER | FLAG_LAVA)
#define FLAG_CRATE   (FLAG_BLOCKED | FLAG_BREAK)
#define TERRAIN_DOOR FLAG_CUSTOM2


#define STATUS_FIRE 1
#define STATUS_BARRIER 2

namespace world {
	void setTerrain(int x, int y, int t);
	unsigned char getTerrain(int x, int y);
	void setTransientTerrain(int x, int y, unsigned char t);
	unsigned char getTransientTerrain(int x, int y);
	bool blocked(int, int);
	bool blockedOrOccupied(int, int);
	bool blockedOneWay(int, int, int, int);
}

#endif
