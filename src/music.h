#ifndef __MUSIC__
#define __MUSIC__

#include <string>

#define MAX_VOLUME 10

namespace music {

	void switchTo(int);

	void playSound(std::string, double pitch = 1);

	void cleanup();

	void setMusicVolume(int);
	void setSoundVolume(int);

	int getMusicVolume();
	int getSoundVolume();

	void onCharacterSelect();
}

#endif
