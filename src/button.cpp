#include "buttons.h"


namespace buttons {
	sf::Texture selectionsTexture;
	sf::Sprite stamp(selectionsTexture);
	
	void stitchLargeTargetTexture(int radius, sf::RenderTexture* rt) {
		
		int texsize = TILESIZE*(radius*2 + 1);
		
		// Default
		stamp.setTextureRect(sf::IntRect(0, 0, TILESIZE, 2*TILESIZE/3));
		stamp.setPosition(radius*TILESIZE, 0);
		rt->draw(stamp);

		stamp.setTextureRect(sf::IntRect(TILESIZE/3, 0, 2*TILESIZE/3, TILESIZE));
		stamp.setPosition(texsize - 2*TILESIZE/3, radius*TILESIZE);
		rt->draw(stamp);

		stamp.setTextureRect(sf::IntRect(0, TILESIZE/3, TILESIZE, 2*TILESIZE/3));
		stamp.setPosition(radius*TILESIZE, texsize - 2*TILESIZE/3);
		rt->draw(stamp);

		stamp.setTextureRect(sf::IntRect(0, 0, 2*TILESIZE/3, TILESIZE));
		stamp.setPosition(0, radius*TILESIZE);
		rt->draw(stamp);

		for (int i=1; i<radius; ++i) {
			stamp.setTextureRect(sf::IntRect(0, 0, 2*TILESIZE/3, 2*TILESIZE/3));
			stamp.setPosition(i*TILESIZE, (radius-i)*TILESIZE);
			rt->draw(stamp);

			stamp.setTextureRect(sf::IntRect(0, TILESIZE/3, 2*TILESIZE/3, 2*TILESIZE/3));
			stamp.setPosition(i*TILESIZE, (radius+i)*TILESIZE + TILESIZE/3);
			rt->draw(stamp);

			stamp.setTextureRect(sf::IntRect(TILESIZE/3, 0, 2*TILESIZE/3, 2*TILESIZE/3));
			stamp.setPosition((radius+i)*TILESIZE + TILESIZE/3, i*TILESIZE);
			rt->draw(stamp);

			stamp.setTextureRect(sf::IntRect(TILESIZE/3, TILESIZE/3, 2*TILESIZE/3, 2*TILESIZE/3));
			stamp.setPosition((radius+i)*TILESIZE + TILESIZE/3, (radius+i)*TILESIZE + TILESIZE/3);
			rt->draw(stamp);
		}
		
		// Highlighted
		stamp.setTextureRect(sf::IntRect(0, TILESIZE, TILESIZE, 2*TILESIZE/3));
		stamp.setPosition(texsize + radius*TILESIZE, 0);
		rt->draw(stamp);
		
		stamp.setTextureRect(sf::IntRect(TILESIZE/3, TILESIZE, 2*TILESIZE/3, TILESIZE));
		stamp.setPosition(2*texsize - 2*TILESIZE/3, radius*TILESIZE);
		rt->draw(stamp);

		stamp.setTextureRect(sf::IntRect(0, 4*TILESIZE/3, TILESIZE, 2*TILESIZE/3));
		stamp.setPosition(texsize + radius*TILESIZE, texsize - 2*TILESIZE/3);
		rt->draw(stamp);

		stamp.setTextureRect(sf::IntRect(0, TILESIZE, 2*TILESIZE/3, TILESIZE));
		stamp.setPosition(texsize, radius*TILESIZE);
		rt->draw(stamp);

		for (int i=1; i<radius; ++i) {
			stamp.setTextureRect(sf::IntRect(0, TILESIZE, 2*TILESIZE/3, 2*TILESIZE/3));
			stamp.setPosition(texsize + i*TILESIZE, (radius-i)*TILESIZE);
			rt->draw(stamp);

			stamp.setTextureRect(sf::IntRect(0, TILESIZE/3*4, 2*TILESIZE/3, 2*TILESIZE/3));
			stamp.setPosition(texsize + i*TILESIZE, (radius+i)*TILESIZE + TILESIZE/3);
			rt->draw(stamp);

			stamp.setTextureRect(sf::IntRect(TILESIZE/3, TILESIZE, 2*TILESIZE/3, 2*TILESIZE/3));
			stamp.setPosition(texsize + (radius+i)*TILESIZE + TILESIZE/3, i*TILESIZE);
			rt->draw(stamp);

			stamp.setTextureRect(sf::IntRect(TILESIZE/3, TILESIZE/3*4, 2*TILESIZE/3, 2*TILESIZE/3));
			stamp.setPosition(texsize + (radius+i)*TILESIZE + TILESIZE/3, (radius+i)*TILESIZE + TILESIZE/3);
			rt->draw(stamp);
		}

		// Pressed
		stamp.setTextureRect(sf::IntRect(0, 2*TILESIZE, TILESIZE, 2*TILESIZE/3));
		stamp.setPosition(radius*TILESIZE, texsize);
		rt->draw(stamp);

		stamp.setTextureRect(sf::IntRect(TILESIZE/3, 2*TILESIZE, 2*TILESIZE/3, TILESIZE));
		stamp.setPosition(texsize - 2*TILESIZE/3, texsize + radius*TILESIZE);
		rt->draw(stamp);

		stamp.setTextureRect(sf::IntRect(0, 7*TILESIZE/3, TILESIZE, 2*TILESIZE/3));
		stamp.setPosition(radius*TILESIZE, 2*texsize - 2*TILESIZE/3);
		rt->draw(stamp);

		stamp.setTextureRect(sf::IntRect(0, 2*TILESIZE, 2*TILESIZE/3, TILESIZE));
		stamp.setPosition(0, texsize + radius*TILESIZE);
		rt->draw(stamp);

		for (int i=1; i<radius; ++i) {
			stamp.setTextureRect(sf::IntRect(0, TILESIZE*2, 2*TILESIZE/3, 2*TILESIZE/3));
			stamp.setPosition(i*TILESIZE, texsize + (radius-i)*TILESIZE);
			rt->draw(stamp);

			stamp.setTextureRect(sf::IntRect(0, TILESIZE/3*7, 2*TILESIZE/3, 2*TILESIZE/3));
			stamp.setPosition(i*TILESIZE, texsize + (radius+i)*TILESIZE + TILESIZE/3);
			rt->draw(stamp);

			stamp.setTextureRect(sf::IntRect(TILESIZE/3, TILESIZE*2, 2*TILESIZE/3, 2*TILESIZE/3));
			stamp.setPosition((radius+i)*TILESIZE + TILESIZE/3, texsize + i*TILESIZE);
			rt->draw(stamp);

			stamp.setTextureRect(sf::IntRect(TILESIZE/3, TILESIZE/3*7, 2*TILESIZE/3, 2*TILESIZE/3));
			stamp.setPosition((radius+i)*TILESIZE + TILESIZE/3, texsize + (radius+i)*TILESIZE + TILESIZE/3);
			rt->draw(stamp);
		}

		rt->display();
	}
	
	std::map<int, sf::RenderTexture*> largeTargetTextures;

	int uiTheme = 0;

	int getUiTheme() {
		return uiTheme;
	}

	void setUiTheme(int uit) {
		uiTheme = uit;
		for(auto itr = textureCache.begin(); itr != textureCache.end(); ++itr) {
			itr->second->loadFromFile("img/uis/" + std::to_string(uit) + "/" + itr->first + ".png");
		}
	}
	
}

Button::Button(sf::IntRect defaultSlice, sf::IntRect highlightSlice, sf::IntRect pressedSlice, sf::IntRect disabledSlice, int x, int y, const std::string& textureFilename, bool themed) : _defaultSlice(defaultSlice), _highlightSlice(highlightSlice), _pressedSlice(pressedSlice), _disabledSlice(disabledSlice), _x(x), _y(y) {
	
	
	
	if(buttons::textureCache.find(textureFilename) == buttons::textureCache.end()){
		sf::Texture* tex = new sf::Texture;
		
		tex->loadFromFile( themed ?
			"img/uis/" + std::to_string(buttons::getUiTheme()) + "/" + textureFilename + ".png" :
			"img/" + textureFilename + ".png"
		);
		
		buttons::textureCache.insert({textureFilename, tex});
	}
	
	sf::Texture* tex = buttons::textureCache[textureFilename];
	sprite = new sf::Sprite(*tex);
	
	sprite->setPosition(x,y);
	sprite->setTextureRect(defaultSlice);
	
	rect = sf::IntRect(x, y, defaultSlice.width, defaultSlice.height);
	
	_disabled = false;
	
}

Button::Button(int tilesetX, int tilesetY, int screenX, int screenY) : Button ( 
		sf::IntRect(tilesetX*MENU_BUTTON_SIZE,tilesetY*4*MENU_BUTTON_SIZE,MENU_BUTTON_SIZE,MENU_BUTTON_SIZE),
		sf::IntRect(tilesetX*MENU_BUTTON_SIZE,(tilesetY*4+1)*MENU_BUTTON_SIZE,MENU_BUTTON_SIZE,MENU_BUTTON_SIZE),
		sf::IntRect(tilesetX*MENU_BUTTON_SIZE,(tilesetY*4+2)*MENU_BUTTON_SIZE,MENU_BUTTON_SIZE,MENU_BUTTON_SIZE),
		sf::IntRect(tilesetX*MENU_BUTTON_SIZE,(tilesetY*4+3)*MENU_BUTTON_SIZE,MENU_BUTTON_SIZE,MENU_BUTTON_SIZE),
		screenX, screenY,
		"buttons",
		true
	) {}
	
Button::Button(int radius, int screenX, int screenY) : _x(screenX), _y(screenY) {

	int texsize = TILESIZE*(radius*2 + 1);

	buttons::largeTargetTextures[radius] = new sf::RenderTexture;
	buttons::largeTargetTextures[radius] -> create(texsize*2, texsize*2);
	buttons::largeTargetTextures[radius] -> clear(sf::Color(0,0,0,0));

	_defaultSlice = sf::IntRect(0, 0, texsize, texsize);
	_highlightSlice = sf::IntRect(texsize, 0, texsize, texsize);
	_pressedSlice = sf::IntRect(0, texsize, texsize, texsize);
	_disabledSlice = sf::IntRect(texsize, texsize, texsize, texsize);

	// Stitch together the button texture
	buttons::stitchLargeTargetTexture(radius, buttons::largeTargetTextures[radius]);

	sprite = new sf::Sprite(buttons::largeTargetTextures[radius]->getTexture());
	sprite->setPosition(_x, _y);

	rect = sf::IntRect(_x, _y, texsize, texsize);

	_disabled = false;
	reset();
}

sf::Sprite* Button::getSprite() {
	//sprite->setTextureRect(_disabled ? _disabledSlice : _highlight ? _highlightSlice : _defaultSlice);
	return sprite;
}

void Button::highlight(){
	if(!_disabled && !_pressed) sprite->setTextureRect(_highlightSlice);
}
void Button::depress(){
	if(!_disabled) {
		sprite->setTextureRect(_pressedSlice);
		_pressed = true;
	}
}
void Button::reset(){
	sprite->setTextureRect(_disabled ? _disabledSlice : _defaultSlice);
	_pressed = false;
}
void Button::disable(bool disable){
	//if(disable) sprite->setTextureRect(_disabledSlice);
	_disabled = disable;
	reset();
}
