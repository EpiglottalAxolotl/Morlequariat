#ifndef __ANIMATIONS__
#define __ANIMATIONS__

#include <SFML/Graphics.hpp>
#include <functional>
#include <memory>

#include "core.h"

namespace animations {
	struct animation {
		int frames;
		int speed;
		bool loop;
		sf::IntRect* frameRects;
		sf::Texture* texture;
	};
	
	void init();
	
	extern std::shared_ptr<animations::animation> takeDamage[NUM_DAMAGE_TYPES]; // TODO add more damage types
	
	// these are sharedptr because of the type signature of addAnimation
	extern std::shared_ptr<animations::animation> gainHealth;
	extern std::shared_ptr<animations::animation> gainSpirit;
	
	
	extern std::shared_ptr<animations::animation> fountain;
	
	extern std::shared_ptr<animations::animation> flowingWater[4]; 
	
	extern animation airBuff; // TODO store all buff animations together
	// TODO don't actually use airBuff as-is, it will crash the system
	
	class AnimationInstance {
		public:
			AnimationInstance(int, int, bool, const std::shared_ptr<animations::animation>&);
			
			const int offsetX;
			const int offsetY;
			const bool atTile;
			
			sf::Sprite* update();
		
		private:
			const std::shared_ptr<animations::animation> a;
			int start;
			int frame;
			sf::Sprite sprite;
	};
	
	class Projectile {
		private:
			int sourceX;
			int sourceY;
			int destX;
			int destY;
			int startTime;
			int duration;
			sf::Sprite sprite;
			
			
		public:
			sf::Sprite* getSprite() { return &sprite; }
			int getCurrentX() const;
			int getCurrentY() const;
			int getDuration() const { return duration; }
			
			bool isDone() const;
			
			Projectile(int, int, int, int, sf::Texture&, int, int);
	};
	
	int animateProjectile(int, int, int, int, damage_type, std::function<void()>);
	int animateItemProjectile(int, int, int, int, int, std::function<void()>);
}

#endif
