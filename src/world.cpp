#include "world.h"
#include "characters.h"
#include "animations.h"
#include "display.h"
#include "triggers.h"
#include "music.h"
#include "cutscenes.h"
#include "enemy.h"

#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

// forward declarations
namespace buttons {
	void setCombat(bool);
	void showMainTurnMenu();
	void showDeathScreen();
	void onEnemyLook(Enemy*);
	void selectCharacter(int);
	bool isTravelMode();
	void restoreDefaultTravelMode();
	int getSelectedCharacter();
}

namespace world {
		
	int width;
	int height;
	int currentId = 101;
	
	int mostRecentCheckpoint;
	
	int saveFile;
	
	bool playerTurn = true;
	bool combat;
	
	struct door {
		int inX;
		int inY;
		int outX;
		int outY;
		int targetMap;
	};
	std::vector<door> doors;
	
	struct lookText {
		int x;
		int y;
		int index;
	};
	std::vector<lookText> lookTexts;
	std::vector<std::shared_ptr<NPC>> npcs;
	
	unsigned char** transientStatus = NULL;
	
	std::map<int, std::shared_ptr<NPC>> npcCache;
	
	struct checkpoint {
		int x;
		int y;
		int charge;
	};

	struct shopItem {
		int shopId;
		ItemOrWeapon* item;
		int price;
		coinType currency;
		int maxStock;
	};
	
	struct room {
		int dungeonId;
		std::vector<Enemy*> enemies;
		std::map<std::pair<int,int>,std::vector<ItemOrWeapon*>*> itemPiles;
		unsigned char** types; // cache types and images because triggers can change them
		unsigned char** world_images;
		std::unordered_set<int> eventFlags;
		int width;
		int height;
		std::vector<int> triggerStates;
		std::vector<checkpoint> checkpoints;
		std::vector<shopItem> shopItems;
		// animations
		// triggers
	};
	std::map<int, room*> allRooms;
	room* currentRoom = NULL;
	
	unsigned char** getAllTileImages() {
		return currentRoom->world_images;
	}
	
	std::unordered_set<int> globalFlags;
	const std::unordered_set<int>& getGlobalFlags(){ return globalFlags; }
	
	struct customTerrainEffect {
		char type;
		int* params;
		bool grounded;
	};
	map<int, vector<customTerrainEffect>> customTerrains;
	
	int getRoomId() { return currentId; }
	
	bool isPlayerTurn() { return playerTurn; }
	
	void clearRoomCache() {
		currentRoom = NULL;
		for (auto itr = allRooms.begin(); itr != allRooms.end(); ++itr) {
			delete itr->second;
		}
		allRooms.clear();
		globalFlags.clear();
	}
	
	void saveRoom(int mapid, room* const r) {
		ofstream tiles("save/"+to_string(saveFile)+"/tiles/"+to_string(mapid), std::ios::binary);
		ofstream stats("save/"+to_string(saveFile)+"/status/"+to_string(mapid), std::ios::binary);
		
		for(int i=0; i<r->height; ++i){
			stats.write((char*) r->types[i], r->width);
			tiles.write((char*) r->world_images[i], r->width);
		}
		
		ofstream data("save/"+to_string(saveFile)+"/data/"+to_string(mapid));
		
		data << r->eventFlags.size();
		
		for(auto itr=r->eventFlags.begin(); itr != r->eventFlags.end(); ++itr) {
			data << " " << *itr;
		}
		data << endl;
		
		for(int i=0; i<r->enemies.size(); ++i) {
			data << "e " << r->enemies[i]->getX() << " " << r->enemies[i]->getY() << " " << r->enemies[i]->getType() << " " << r->enemies[i]->getDrop() << " " << r->enemies[i]->getHealth() << std::endl;
		}
		
		for(auto itr = r->itemPiles.cbegin(); itr != r->itemPiles.cend(); ++itr) {
			std::vector<ItemOrWeapon*>* piles = itr->second;
			for(int i=0; i<piles->size(); ++i) {
				data << (*piles)[i]->serialize();
				data << " " << itr->first.first << " " << itr->first.second << std::endl;
			}
		}

		for(int i=0; i<r->shopItems.size(); ++i) {
			int id = r->shopItems[i].item->getID();
			if (r->shopItems[i].item->getPickupableType() == ptWeapon) id *= -1;
			data << "s " << r->shopItems[i].shopId << " " << id << " " << r->shopItems[i].price << " " << (int)(r->shopItems[i].currency) << " " << r->shopItems[i].maxStock << std::endl;
		}
		
		for(int i=0; i<r->checkpoints.size(); ++i) {
			data << "c " << r->checkpoints[i].x << " " << r->checkpoints[i].y << " " << r->checkpoints[i].charge << std::endl;
		}
		
		for(int i=0; i<r->triggerStates.size(); ++i) {
			data << "t " << i << " " << r->triggerStates[i] << std::endl;
		}
	}
	
	void deleteRoomCache(int mapid, room* r) {		
		for(int i = 0; i<r->enemies.size(); ++i) {
			if(r->enemies[i]->getDrop() && r->enemies[i]->getDrop()->getPickupableType() == ptCoin) delete r->enemies[i]->getDrop();
			delete r->enemies[i];
		}
		r->enemies.clear();
		
		for(auto itr = r->itemPiles.begin(); itr != r->itemPiles.end(); ++itr) {
			// Don't delete the contents of the vector! They're pointers to the global list of all items.
			delete itr->second;
		}
		
		for(int i=0; i < r->height; ++i) delete[] r->types[i];
		for(int i=0; i < r->height; ++i) delete[] r->world_images[i];
		delete[] r->types;
		delete[] r->world_images;
		
		delete r;
	}
	
	void resetRoom(int mapid, room* r) {
		ifstream levelDataIn("map/data/"+to_string(mapid));
		ofstream levelDataOut("save/"+to_string(saveFile)+"/data/"+to_string(mapid));
		stringstream lineStream;
		string line;
		getline(levelDataIn, line);
		
		int tempWidth;
		int tempHeight;
		
		lineStream.str(line);
		lineStream >> tempWidth >> tempHeight;
		// This is room-level data (bounds, music, etc). Not relevant here.
				
		// Originally, I was going to transform the old room-in-memory structure into a new room-in-memory structure.
		// But it's easier to just take everything from the room we want to keep, write it to disk, and forget the rest.
		// If the player goes back into the dungeon, read the disk again, no problem.

		levelDataOut << 0 << endl; // level flags

		// Look for enemies that are holding non-respawnable items; reset their health and location.
		// TODO this won't actually work either! the spawn coordinates may be inaccurate if the enemies were read from a save file
		for(int i=0; i<r->enemies.size(); ++i) {
			if(r->enemies[i]->getDrop() && r->enemies[i]->getDrop()->isPermanent()) {
				levelDataOut << r->enemies[i]->getSpawnX() << " " << r->enemies[i]->getSpawnY() << " " << r->enemies[i]->getType() << " " << r->enemies[i]->getDrop()->serialize() << std::endl;
			}
		}

		while(getline(levelDataIn, line)){
			lineStream.str(line);
			lineStream.clear();
			char type;
			lineStream >> type;
			
			switch(type){
				case 'i': {
					int _;
					int id;
					lineStream >> _ >> _ >> id;
					
					// Non-permanent items (i.e. consumables) respawn.
					if(!items::getItem(id)->isPermanent()) levelDataOut << line << endl;
					break;
				}
				// Assume that weapons aren't intrinsic to puzzles and therefore shouldn't ever reset
				case 'e': {
					int x, y, id;
					lineStream >> x >> y >> id;
					
					// TODO check to see if an enemy has already been placed at those coordinates
					// and if so, skip this enemy, because it means that this enemy has already respawned

					string dropString;
					lineStream >> type;
					switch(type) {
						case 'i': {
							int itemId;
							lineStream >> itemId;
							item* i = items::getItem(itemId);
							if(i->isPermanent()) {
								dropString = "-";
							} else {
								dropString = i->serialize();
							}
							break;
						}
						case 'w': {
							dropString = "-";
							break;
						}
						case '$': {
							int type, count;
							lineStream >> type >> count;
							if ((coinType)type == ctZinc) {
								dropString = "$ 0 " + to_string(count);
							} else {
								dropString = "-";
							}
							break;
						}
						default: dropString = "-";
					}
					levelDataOut << x << " " << y << " " << id << " " << dropString << endl;
					break;
				}
				
				case 'c' : {
					levelDataOut << line << endl;
					break;
				}

				case '$' : {
					int _;
					int type;
					lineStream >> _ >> _ >> type;
					if ((coinType)type == ctZinc) levelDataOut << line << endl;
					break;
				}
			}
		}
		
		// Permanent items not native to the room; the only reason why this doesn't just delete the cache
		for(auto itr = r->itemPiles.cbegin(); itr != r->itemPiles.cend(); ++itr) {
			std::vector<ItemOrWeapon*>* pile = itr->second;
			for(int i=0; i<pile->size(); ++i) {
				if ((*pile)[i]->isPermanent()) {
					levelDataOut << (*pile)[i]->serialize() << " " << itr->first.first << " " << itr->first.second << std::endl;
				}
			}
		}
		
		deleteRoomCache(mapid, r);
		
		remove(("save/"+to_string(saveFile)+"/tiles/"+to_string(mapid)).c_str());
		remove(("save/"+to_string(saveFile)+"/status/"+to_string(mapid)).c_str());
		
	}
	
	void saveRooms() {
		auto itr = allRooms.begin();
		while(itr != allRooms.end()){
			std::cout << "Saving room " << itr->first << std::endl;
			if(itr->second->dungeonId && !(currentRoom->dungeonId)) {
				resetRoom(itr->first, itr->second);
				itr = allRooms.erase(itr);
			} else {
				saveRoom(itr->first, itr->second);
				++itr;
			}
		}
	}
	

	const std::vector<Enemy*>& getEnemies(){
		return currentRoom->enemies;
	}
	
	const std::vector<std::shared_ptr<NPC>>& getNPCs(){
		return npcs;
	}
	
	void addNpc(int x, int y, int id, int ds){
		auto itr = npcCache.find(id);
		if (itr == npcCache.end()) {
			npcCache[id] = std::shared_ptr<NPC> { new NPC(id) };
		}
		std::shared_ptr<NPC> npc = npcCache[id];
		npc->setXY(x, y);
		npc->setDialogueStart(ds);
		npcs.push_back(npc);
	}
	
	void loadNew(int sf) {
		saveFile = sf;
		loadMap(0);
		characters::startPlayerTurn();
	}
	
	void setFileId(int fileId) {
		saveFile = fileId;
	}
	
	void cleanup() {
		triggers::clear();
		display::clearAnimations();
		doors.clear();
		lookTexts.clear();
		npcs.clear();
		
		if(transientStatus) {
			for(int i=0; i<height; ++i) {
				delete[] transientStatus[i];
			}
			delete[] transientStatus;
			transientStatus = 0;
		}
		
		for(auto itr = customTerrains.begin(); itr != customTerrains.end(); ++itr) {
			for(int i=0; i<itr->second.size(); ++i) delete[] itr->second[i].params;
		}
		customTerrains.clear();
	}
	
	void loadMap(int mapid){
		
		// cache metatrigger states
		if(currentRoom) currentRoom->triggerStates = triggers::listCachedMetaTriggers();
		
		cleanup();
		
		ifstream leveldata("map/data/"+to_string(mapid));
		
		stringstream lineStream;
		string line;
		getline(leveldata, line);
		lineStream.str(line);
		
		int tileset;
		int defaultMusic;
		int dungeonId;
		lineStream >> width >> height >> tileset >> defaultMusic >> dungeonId;

		music::switchTo(defaultMusic);

		vector<shared_ptr<animations::animation>> animationTypes;
		
		bool cached = false;
		if(allRooms.find(mapid) != allRooms.end()){
			currentRoom = allRooms[mapid];
			cached = true;
		} else {
			
			allRooms[mapid] = new room;
			allRooms[mapid] -> dungeonId = dungeonId;
			allRooms[mapid] -> width = width;
			allRooms[mapid] -> height = height;
			currentRoom = allRooms[mapid];
			
			std::cout << "Loading map " << mapid << std::endl;;

			ifstream tiles("save/"+to_string(saveFile)+"/tiles/"+to_string(mapid), std::ios::binary);
			ifstream stats("save/"+to_string(saveFile)+"/status/"+to_string(mapid), std::ios::binary);
			
			if(!tiles.is_open()) tiles = ifstream("map/tiles/"+to_string(mapid), std::ios::binary);
			if(!stats.is_open()) stats = ifstream("map/status/"+to_string(mapid), std::ios::binary);
			
			currentRoom->types = new unsigned char*[height];
			currentRoom->world_images = new unsigned char*[height];
			for(int i=0; i<height; ++i){
				char* typeRow = new char[width];
				char* tileRow = new char[width];
				stats.read(typeRow, width);
				tiles.read(tileRow, width);
				currentRoom->types[i] = (unsigned char*) typeRow;
				currentRoom->world_images[i] = (unsigned char*) tileRow;
			}
			currentRoom->eventFlags.clear();
		}
		
		cutscenes::loadRoomDialogue(mapid);
		
		// Initialize new transient status array
		transientStatus = new unsigned char* [height];
		for(int i=0; i<height; ++i) {
			transientStatus[i] = new unsigned char[width];
			for (int j=0; j<width; ++j) transientStatus[i][j] = 0;
		}
		
		ifstream savedLevelData("save/"+to_string(saveFile)+"/data/"+to_string(mapid));
		
		bool saved = false;
		if(!cached && savedLevelData.is_open()) {
			saved = true;
			
			// Need to read the first line of the saved room file to get flags, in case any load triggers depend on them
			
			getline(savedLevelData, line);
			
			lineStream.str(line);
			lineStream.clear();
				
			int flagCount;
			lineStream >> flagCount;
			
			for(int i=0; i<flagCount; ++i) {
				int temp;
				lineStream >> temp;
				cout << "Read flag " << temp << endl;
				currentRoom->eventFlags.insert(temp);	
			}
		}

		while(getline(leveldata, line)){
		
			if(line.length() == 0) continue;
			lineStream.str(line);
			lineStream.clear();
	//		string type;
			char type;
			lineStream >> type;
			switch(type){
				case 'A': {// Define animation type
					std::shared_ptr<animations::animation> a { new animations::animation };
					// TODO cache textures
					// filename width height y-offset framecount
					a->texture = new sf::Texture;
					string textureFilename;
					lineStream >> textureFilename;
					a->texture->loadFromFile("img/"+textureFilename+".png");
					a->loop = true;
					int w,h,x,y;
					lineStream >> w >> h >> y >> a->frames >> a->speed;
					
					a->frameRects = new sf::IntRect[a->frames];
					std::cout << "frame count: " << a->frames << endl;
					for(int i=0;i<a->frames;++i) a->frameRects[i] = sf::IntRect(w*i, y, w, h);
					animationTypes.push_back(a);
					break;
				}
				case 'T': {// Define custom terrain effect
					int terrainId;
					lineStream >> terrainId;
					std::vector<customTerrainEffect> effects;
					while(lineStream.rdbuf()->in_avail()) {
						customTerrainEffect effect;
						lineStream >> effect.type;
						
						int params;
						switch(effect.type) {
							case 'd': params = 2; break; // damage
							case 'e': params = 2; break; // damage enemies only
							case 's': params = 2; break; // status condition
							case 'g': params = 0; break; // effect is limited to ground
							case 't': params = 1; break; // allow-throw (-1 allows all items)
						}
						effect.params = new int[params];
						for(int i=0; i<params;++i) lineStream >> effect.params[i];
						
						effects.push_back(effect);
					}
					customTerrains[terrainId] = effects;
					break;
				}
				case 'i': {// define item
					if(cached || saved) break;
					int x;
					int y;
					int id;
					lineStream >> id >> x >> y;
					putItem(x,y,items::getItem(id));
					break;
				}
				case 'w': {
					if(cached || saved) break;
					int x;
					int y;
					int id;
					lineStream >> id >> x >> y;
					putItem(x,y,weapons::getWeapon(id));
					break;
				}
				case '$': {
					if(cached || saved) break;
					int x;
					int y;
					int id;
					int count;
					lineStream >> id >> count >> x >> y;
					putItem(x,y, new coinPile((coinType)id, count));
					break;
				}
				case 'e': {
					if(cached || saved) break;
					int x;
					int y;
					int id;
					lineStream >> x >> y >> id;

					ItemOrWeapon* drop = NULL;

					lineStream >> type;
					switch(type) {
						case 'i': {
							int itemId;
							lineStream >> itemId;
							drop = items::getItem(itemId);
							break;
						}
						case 'w': {
							int weapId;
							lineStream >> weapId;
							drop = weapons::getWeapon(weapId);
							break;
						}
						case '$': {
							int type, count;
							lineStream >> type >> count;
							drop = new coinPile((coinType)type, count);
							break;
						}
					}
					
					putEnemy(x, y, id, drop);
					break;
				}
				case 'n': {
					int x;
					int y;
					int id;
					int ds; // dialogue start
					lineStream >> x >> y >> id >> ds;
					addNpc(x, y, id, ds);
					break;
				}	
				case 'd' : {
					door d;
					lineStream >> d.inX >> d.inY >> d.targetMap >> d.outX >> d.outY;
					doors.push_back(d);
					break;
				}
				/*case 'D': {
					// multiple offscreen doors
					direction dir;
					int min, length, roomid, offset, bound;
					lineStream >> dir >> min >> length >> roomid >> offset >> bound;
					
					for (int i=0; i<length; ++i) {
						door d;
						
						switch(dir) {
							case _up:
								d.inX = min + i;
								d.inY = -1;
								d.outX = min + i + offset;
								d.outY = bound;
								break;
							case _down:
								d.inX = min + i;
								d.inY = height;
								d.outX = min + i + offset;
								d.outY = 0;
								break;
							case _left:
								d.inX = -1;
								d.inY = min + i;
								d.outX = bound;
								d.outY = min + i + offset;
								break;
							case _right:
								d.inX = width;
								d.inY = min + i;
								d.outX = 0;
								d.outY = min + i + offset;
								break;
						}
						
						d.inX =  
						d.inY =
						d.targetMap = roomid;
						d.outX = 
						d.outY =
						doors.push_back(d);
					}
					break;
				}*/
				case 'a' : {
					int x,y,id;
					lineStream >> x >> y >> id;
					if(id < animationTypes.size()) {
						display::addAnimationAtTile(x,y,animationTypes[id], true);
					} else {
						cerr << "Animation index out of range: " << id << endl;
					}
					break;
				}
				case 'b' : { // background animation
					int x,y,id;
					lineStream >> x >> y >> id;
					if(id < animationTypes.size()) {
						display::addAnimationAtTile(x,y,animationTypes[id], false);
					} else {
						cerr << "Animation index out of range: " << id << endl;
					}
					break;
				}
				case 't': {
					triggers::parseTrigger(lineStream);
					break;
				}
				case 'l': {
					lookText lt;
					lineStream >> lt.x >> lt.y >> lt.index;
					lookTexts.push_back(lt);
					break;
				}
				case 'c': {
					if(cached || saved) break;
					checkpoint cp;
					lineStream >> cp.x >> cp.y >> cp.charge;
					currentRoom->checkpoints.push_back(cp);
					
					if(cp.charge > 0x01) { // more than just save
						display::addAnimationAtTile(cp.x,cp.y, animations::fountain );
					}
					break;
				}
				case 's': {
					if(cached || saved) break;
					int currency;
					int id;
					shopItem si;
					lineStream >> si.shopId >> id >> si.price >> currency >> si.maxStock;
					if (id >= 0) {// this means that weapons with id 0 can't be sold - fix later
						si.item = items::getItem(id);
					} else {
						si.item = weapons::getWeapon(-id);
					}
					si.currency = (coinType) currency;
					currentRoom->shopItems.push_back(si);
					break;
				}
				default:
					cerr << "Cannot parse line in world data file (starting with character " << (int)type << "): " << line << endl;
			}
		}
		if(cached) {
			for(int i=0; i<currentRoom->triggerStates.size(); ++i) {
				triggers::initializeCachedMetaTrigger(i, currentRoom->triggerStates[i]);
			}
			
			for(int i=0; i<currentRoom->checkpoints.size(); ++i) {
				if(currentRoom->checkpoints[i].charge > 0x01) { // more than just save
					display::addAnimationAtTile(currentRoom->checkpoints[i].x,currentRoom->checkpoints[i].y, animations::fountain );
				}
			}
			
		} else if(saved) {
			cout << "Loading saved level data for level " << mapid << endl;

			while(getline(savedLevelData, line)){
				lineStream.str(line);
				lineStream.clear();
				char type;
				lineStream >> type;
				
				switch(type){
					case 'i': {// define item
						int x;
						int y;
						int id;
						lineStream >> id >> x >> y;
						putItem(x,y,items::getItem(id));
						break;
					}
					case 'w': {
						int x;
						int y;
						int id;
						lineStream >> id >> x >> y;
						putItem(x,y,weapons::getWeapon(id));
						break;
					}
					case '$': {
						int x;
						int y;
						int id;
						int count;
						lineStream >> id >> count >> x >> y;
						putItem(x,y, new coinPile((coinType)id, count));
						break;
					}
					case 's': {
						int currency;
						int id;
						shopItem si;
						lineStream >> si.shopId >> id >> si.price >> currency >> si.maxStock;
						if (id >= 0) {// this means that weapons with id 0 can't be sold - fix later
							si.item = items::getItem(id);
						} else {
							si.item = weapons::getWeapon(-id);
						}
						si.currency = (coinType) currency;
						currentRoom->shopItems.push_back(si);
						break;
					}
					case 'e': {
						int x;
						int y;
						int id;
						int hp;
						lineStream >> x >> y >> id >> hp;

						ItemOrWeapon* drop = NULL;

						lineStream >> type;
						switch(type) {
							case 'i': {
								int itemId;
								lineStream >> itemId;
								drop = items::getItem(itemId);
								break;
							}
							case 'w': {
								int weapId;
								lineStream >> weapId;
								drop = weapons::getWeapon(weapId);
								break;
							}
							case '$': {
								int type, count;
								lineStream >> type >> count;
								drop = new coinPile((coinType)type, count);
								break;
							}
						}

						Enemy* enemy = new Enemy(x,y,id, drop);
						if(hp > 0) enemy->setHealth(hp);
						currentRoom->enemies.push_back(enemy);
						break;
					}
					case 't': {
						int id;
						int state;
						lineStream >> id >> state;
						triggers::initializeCachedMetaTrigger(id, state);
						break;
					}
					case 'c': {
						checkpoint cp;
						lineStream >> cp.x >> cp.y >> cp.charge;
						currentRoom->checkpoints.push_back(cp);
						
						if(cp.charge > 0x01) { // more than just save
							display::addAnimationAtTile(cp.x,cp.y, animations::fountain );
						}
						break;
					}
					default:
						cerr << "Cannot parse line in saved room data file (starting with character " << (int)type << "): " << line << endl;
				}
			}
		}
		
		currentId = mapid;
		
		display::loadBackground(currentRoom->world_images, currentRoom->types, width, height, tileset);
		
		if (currentRoom->enemies.empty()) {
			buttons::restoreDefaultTravelMode();
			enableCombat(false);	
		} else {
			enableCombat(true);
		}
	}
	
	void setTerrain(int x, int y, int t){
		int tOld = currentRoom->types[y][x];
		currentRoom->types[y][x] = t;
		
		if(tOld & FLAG_BREAK) {
			display::scheduleSetTile(x, y, currentRoom->world_images[y][x]);
		} else if((tOld & (FLAG_ONEWAY | FLAG_BLOCKED)) == FLAG_ONEWAY) {
			std::cout << "Removing flowing water animation" << std::endl;
			display::removeAnimationAtTile(x, y, false);
		}
		
		if((t & FLAG_CRATE) == FLAG_CRATE) {
			// Breakable object (crate, mushroom)
			display::scheduleSetTile(x, y, CRATE_TILE);
		} else if((t & FLAG_CRATE) == FLAG_BREAK) {
			// Breakable floor (lilypad)
			display::scheduleSetTile(x, y, FLOOR_BREAK_TILE);
		} else if((t & (FLAG_ONEWAY | FLAG_BLOCKED)) == FLAG_ONEWAY) {
			// Flowing water
			direction pushDir = (direction) ((t >> 6)%4);
			StattedCharacter* target = getCharacterAt(x, y);
			if(target) target->onPush(pushDir, 1, 0);
			
			display::addAnimationAtTile(x, y, animations::flowingWater[pushDir], false);
		}
		
		if((t & FLAG_CUSTOM) == FLAG_CUSTOM && t != tOld) {
			StattedCharacter* c = getCharacterAt(x, y);
			if(c) handleCustomTerrain(t, c);
		}
		
	}
	
	void setTransientTerrain(int x, int y, unsigned char t) {
		if(x<0 || y<0 || x>=width || y>=height) return;
		transientStatus[y][x] = t;
		StattedCharacter* character = getCharacterAt(x, y);
		if (character) handleTransientTerrain(t, character);
	}
	
	void setTileImage(int x, int y, int t){
		currentRoom->world_images[y][x] = t;
		display::scheduleSetTile(x, y, t);
	}
	
	unsigned char getTerrain(int x, int y){
		if(x<0 || y<0 || x>=width || y>=height) return 0;
		return currentRoom->types[y][x];
	}
	
	unsigned char getTransientTerrain(int x, int y) {
		if(x<0 || y<0 || x>=width || y>=height) return 0;
		return transientStatus[y][x];
	}
	
	bool blockedOneWay(int startX, int startY, int endX, int endY) {
		int terrain = getTerrain(endX, endY);
		if((terrain & (FLAG_ONEWAY | FLAG_BLOCKED)) == (FLAG_ONEWAY | FLAG_BLOCKED)) {
			direction dir = (direction) ((terrain >> 6)%4);
			switch(dir) {
				case _left:
					return startX < endX;
				case _right:
					return startX > endX;
				case _up:
					return startY < endY;
				case _down:
					return startY > endY;
			}
		} else {
			return false;
		}
	}
	
	int getTileImage(int x, int y){ // probably not recommended
		if(x<0 || y<0 || x>=width || y>=height) return 0;
		return currentRoom->world_images[y][x];
	}
	
	void enableCombat(bool _combat) {
		combat = _combat;
		buttons::setCombat(combat);
		if(!combat) {
			for(int i=0; i<characters::playable.size(); ++i) {
				characters::playable[i]->setActed(false);
				characters::playable[i]->setMoved(false);
			}
		}
	}
	
	bool isCombat() {
		return combat;
	}
	
	void damageAtTile(int x, int y, int amt, damage_type dtype) {
		display::addAnimationAtTile(x,y, animations::takeDamage[dtype] );
		StattedCharacter* target = getCharacterAt(x, y);
		if(target){
			target->takeDamage(amt, dtype);
		} else {
			damageTerrain(x, y, amt, dtype);			
		}
	}
	
	void damageTerrain(int x, int y, int amt, damage_type dtype) {
		if(x<0 || y<0 || x>=width || y>=height) return;
		triggers::processGeographicTrigger(triggers::input_type::damageType, x, y, dtype);
		if(transientStatus[y][x] == STATUS_BARRIER) {
			setTransientTerrain(x, y, 0);
		}
		if((currentRoom->types[y][x] & FLAG_CRATE) == FLAG_CRATE) {
			music::playSound(to_string(10 + dtype));
			setTerrain(x, y, currentRoom->types[y][x] & ~FLAG_CRATE);
		}
	}
	
	void onWaterHit(int x, int y) {
		// Maybe play a water sound effect here? TODO
		setTransientTerrain(x, y, 0);
	}

	void handleCustomTerrain(int terrainId, StattedCharacter* character) {
		auto itr = customTerrains.find(terrainId);
		if(itr == customTerrains.end()) return;
		for (customTerrainEffect effect : itr->second) {
			switch(effect.type) {
				case 'e': {
					Enemy* enemy = dynamic_cast<Enemy*>(character);
					if(!enemy) break;
					// FALLTHRU
				}
				case 'd': {
					character->takeDamage(effect.params[0], (damage_type) effect.params[1]);
					display::addAnimationAtTile(character->getX(),character->getY(), animations::takeDamage[effect.params[1]] );
					break;
				}
				case 's': {
					character->addCondition((conditionId) effect.params[0], effect.params[1]);
					break;
				}
				case 'g': {
					if (!character->isTouchingGround()) return;
					break;
				}
			}
		}
	}

	void handleTransientTerrain(unsigned char terrain, StattedCharacter* character) {
		switch(terrain) {
			case STATUS_FIRE:
				character->addCondition(burning,7);
				break;
		}
	}
	
	void putItem(int x, int y, ItemOrWeapon* i){
		std::cout << "putting " << i->getName() << " at " << x << ", " << y << std::endl;
		auto pair = std::make_pair(x,y);
		if(currentRoom->itemPiles.find(pair) == currentRoom->itemPiles.end()){
			currentRoom->itemPiles[pair] = new std::vector<ItemOrWeapon*>;
		}
		currentRoom->itemPiles[pair]->push_back(i);
		if(i->getPickupableType() == ptItem) triggers::processGeographicTrigger(triggers::input_type::itemPut, x, y, i->getID());
	}
	void removeNthItemAt(int x, int y, int i){
		auto pair = std::make_pair(x,y);
		ItemOrWeapon* it = (*currentRoom->itemPiles[pair])[i];
		if(it->getPickupableType() == ptItem) triggers::processGeographicTrigger(triggers::input_type::itemTake, x, y, it->getID());
		else if(it->getPickupableType() == ptCoin) {
			delete it;
		}
		currentRoom->itemPiles[pair]->erase(currentRoom->itemPiles[pair]->begin() + i);
		if(currentRoom->itemPiles[pair]->size() == 0){
			delete currentRoom->itemPiles[pair];
			currentRoom->itemPiles.erase(pair);
		}
	}
	int countItemsAt(int x, int y){
		if(currentRoom->itemPiles.find(std::make_pair(x,y)) == currentRoom->itemPiles.end()) return 0;
		return currentRoom->itemPiles[std::make_pair(x,y)]->size();
	}
	const std::vector<ItemOrWeapon*>& getItemsAt(int x, int y){
		return *currentRoom->itemPiles[std::make_pair(x,y)];
	}
	
	int getDoorIndex(int x, int y) {
		for(int i=0; i<doors.size(); ++i) {
			if(doors[i].inX == x && doors[i].inY == y){
				return i;
			}
		}
		return -1;
	}
	
	void processDoor(PlayerCharacter* pc, int x, int y){
		int i = getDoorIndex(x, y);
		
		if(i < 0) return;
		// have to cache these so they don't get overwritten during loadMap
		int newX = doors[i].outX;
		int newY = doors[i].outY;
		int newRoom = doors[i].targetMap;
		
		pc -> setRoomId(newRoom);
		
		if(isCombat() || !buttons::isTravelMode()) {
			for(int j=0; j<characters::playable.size(); ++j) {
				if(characters::playable[j]->getRoomId() != newRoom && characters::playable[j]->getHealth() > 0) {
					buttons::selectCharacter(j);
					return;
				}
			}
		}
		
		// now that we've determined all the characters are indeed going into the new room
		// we position them within the new room
		
		for(int j=0; j<characters::playable.size(); ++j) {
			if(buttons::isTravelMode() && j != buttons::getSelectedCharacter()) {
				characters::playable[j]->setXY(-1, -1);
			} else {
				characters::playable[j]->setXY(newX, newY);
			}
			if(characters::playable[j]->getHealth() == 0 || buttons::isTravelMode()) characters::playable[j]->setRoomId(newRoom);
		}
		
		loadMap(doors[i].targetMap);
		characters::startPlayerTurn();
		
		return;
	}
	
	bool hasRoomFlag(int flag) {
		return currentRoom->eventFlags.find(flag) != currentRoom->eventFlags.end();
	}
	
	void setRoomFlags(int flag) {
		currentRoom->eventFlags.insert(flag);
	}
	
	bool hasGlobalFlag(int flag) {
		return globalFlags.find(flag) != globalFlags.end();
	}
	
	void addGlobalFlag(int flag) {
		globalFlags.insert(flag);
	}
	
	void removeGlobalFlag(int flag) {
		globalFlags.erase(flag);
	}
	
	int getDungeonId() {
		return currentRoom->dungeonId;
	}
	
	int itemPileSpriteIndex(int x, int y){
		int index = 0;

		auto items = currentRoom->itemPiles.find(std::make_pair(x,y));
		if(items != currentRoom->itemPiles.end()){
			for(int i=0; i<items->second->size(); ++i){
				switch((*items->second)[i]->getPickupableType()) {
					case ptItem:
						index |= (1 << items::itemCategoryToSpriteIndex(static_cast<item*>((*items->second)[i])->category));
						break;
					case ptWeapon:
						switch(static_cast<weapon*>((*items->second)[i])->wclass){
							case 0: index |= 0x10; break;
							case 1: index |= 0x01; break;
							default: index |= 1;
						}
						break;
					case ptCoin:
						index |= (static_cast<coinPile*>((*items->second)[i])->getCoinType() + 1) << 8;
						break;
				}
			}
		}
		
		return index;
	}
	
	StattedCharacter* getCharacterAt(int x, int y){
		for(int i=0; i<characters::playable.size(); ++i){
			if(characters::playable[i]->getX() == x && characters::playable[i]->getY() == y && characters::playable[i]->getRoomId() == currentId && characters::playable[i]->getHealth() > 0) {
				return characters::playable[i];
			}
		}
		return getEnemyAt(x,y);
	}
	
	Enemy* getEnemyAt(int x, int y){
		const std::vector<Enemy*>& roomEnemies = getEnemies();
		for(int i=0; i<roomEnemies.size(); ++i){
			if(roomEnemies[i]->getX() == x && roomEnemies[i]->getY() == y) {
				if(roomEnemies[i]->getHealth() <= 0) return NULL; // dead enemies don't count
				return roomEnemies[i];
			} else if(roomEnemies[i]->hasFlags(ENEMY_LARGE)) {
				int largeEnemyX = roomEnemies[i]->getX();
				int largeEnemyY = roomEnemies[i]->getY();
				if( (x == largeEnemyX || x == largeEnemyX + 1) && (y == largeEnemyY || y == largeEnemyY + 1) ) {
					return roomEnemies[i];
				}
			}
		}
		return NULL;
	}
	
	std::shared_ptr<NPC> getNPCAt(int x, int y) {
		for(int i=0; i<npcs.size(); ++i){
			if(npcs[i]->getX() == x && npcs[i]->getY() == y) {
				return npcs[i];
			}
		}
		return nullptr;
	}
	
	Enemy* putEnemy(int x, int y, int id, ItemOrWeapon* drop) {
		Enemy* enemy = new Enemy(x,y,id,drop);
		currentRoom->enemies.push_back(enemy);
		triggers::processGeographicTrigger(triggers::input_type::enemyEnter, x, y, id);
		return enemy;
	}
	
	void onEnemyDeath() {
		if(playerTurn) {
			bool allEnemiesDead = true;
			for(int i=0; i<currentRoom->enemies.size(); ++i) {
				if(currentRoom->enemies[i]->getHealth() > 0) allEnemiesDead = false;
			}
			if(allEnemiesDead) {
				currentRoom->enemies.clear();
				enableCombat(false);
			}
		}
	}
	
	void startEnemyTurn() {
		// clear out dead enemies
		for(auto itr=currentRoom->enemies.begin(); itr!=currentRoom->enemies.end();){
			if((*itr)->getHealth() <= 0){
				Enemy* temp = *itr;
				itr = currentRoom->enemies.erase(itr);
				delete temp;
			} else ++itr;
		}
		
		if(currentRoom->enemies.size() > 0) {
			playerTurn = false;
			
			triggers::processTurnStartTriggers();
		
			for(int i=0; i<currentRoom->enemies.size(); ++i){
				currentRoom->enemies[i]->specificBeginTurn();
			}
			
		} else {
			enableCombat(false);
			buttons::showMainTurnMenu();
		}
	}
	
		
	bool playerTurnDone() {
		for(int i=0; i<characters::playable.size(); ++i){
			if(! characters::playable[i]->isDone() ) return false;
		}

		return true;
	}
	
	void handlePlayerDeath() {
		auto itr = allRooms.begin();
		while(itr != allRooms.end()){
			if(itr->second == currentRoom) {
				++itr;
			} else {
				deleteRoomCache(itr->first, itr->second);
				itr = allRooms.erase(itr);
			}
		}
		buttons::showDeathScreen();
	}
	
	void checkPlayerDeath() {
		bool dead = true;
		for(int i=0; i<characters::playable.size(); ++i) {
			if(characters::playable[i]->getHealth() > 0) dead = false;
		}
		if(dead) {
			handlePlayerDeath();
		}
	}
	
	void checkPlayerTurn() {
		if(playerTurn) return;
		for(int i=0; i<currentRoom->enemies.size(); ++i){
			if(!currentRoom->enemies[i]->isDone()) return;
		}
		playerTurn = true;
		characters::startPlayerTurn();
	}
	
	void onRespawn() {
		deleteRoomCache(currentId, currentRoom);
		allRooms.erase(currentId);
		savefile::load(saveFile);
		display::clearView();
	}
	
	bool outOfBounds(int x, int y) {
		return (x<0 || y<0 || x>=width || y>=height);
	}
	
	bool blocked(int x, int y){
		if(outOfBounds(x, y)) return true;
		return ((currentRoom->types[y][x] & (FLAG_BLOCKED | FLAG_ONEWAY)) == FLAG_BLOCKED)
		    || (transientStatus[y][x] == STATUS_BARRIER);
	}
	
	bool blockedOrOccupied(int x, int y){
		return blocked(x, y) || !!getCharacterAt(x,y);
	}
	
	bool isThrowTarget(int itemIndex, int x, int y) {
		if(outOfBounds(x, y)) return false;
		if(currentRoom->types[y][x] & FLAG_CUSTOM){
			auto itr = customTerrains.find(currentRoom->types[y][x]);
			if(itr != customTerrains.end()){
				for (customTerrainEffect effect : itr->second) {
					if(effect.type == 't' && (effect.params[0]==itemIndex || effect.params[1]==-1)) return true;
				}
			}
		}
		return !( (currentRoom->types[y][x] & FLAG_CRATE) == FLAG_BLOCKED );
	}
	
	int collectSpellItem(int x, int y) {
		int transient = getTransientTerrain(x, y);
		setTransientTerrain(x, y, 0);
		switch(transient) {
			case STATUS_FIRE:
				return 39;
			case STATUS_BARRIER:
				return 40;
			case 0:
				// There will be a way of adding custom terrains that give a specific item when COLLECTed.
				switch(getTerrain(x, y) & LIQUID_MASK) {
					case FLAG_WATER:
						return 9;
					case FLAG_LAVA:
						return 10;
					default:
						return -1;
				}
		}
	}

	bool hasLookText(int x, int y) {
		for(int i=0; i<lookTexts.size(); ++i) if(lookTexts[i].x==x && lookTexts[i].y==y) {
			return true;
		}
		return false;
	}

	bool hasCheckpoint(int x, int y) {
		for(int i=0; i<currentRoom->checkpoints.size(); ++i) {
			if(currentRoom->checkpoints[i].x == x && currentRoom->checkpoints[i].y == y) {
				return true;
			}
		}
		return false;
	}

	bool onLook(int x, int y){
		for(int i=0; i<currentRoom->checkpoints.size(); ++i) {
			if(currentRoom->checkpoints[i].x == x && currentRoom->checkpoints[i].y == y) {
				mostRecentCheckpoint = i;
				cutscenes::loadDialogueFile(-1);
				cutscenes::startDialogue(currentRoom->checkpoints[i].charge);
				return true;
			}
		}
		std::shared_ptr<NPC> npc = getNPCAt(x, y);
		if(npc) {
			npc->startDialogue();
			return true;
		}
		Enemy* e = getEnemyAt(x,y);
		if(e && !e->hasFlags(ENEMY_INVIS)) {
			buttons::onEnemyLook(e);
			return true;
		} else for(int i=0; i<lookTexts.size(); ++i) if(lookTexts[i].x==x && lookTexts[i].y==y) {
			cutscenes::startDialogue(lookTexts[i].index);
			return true;
		}
		return false;
	}
	
	void handleCheckpoint(int charge) {
		if(charge & 0x02) { // health
			for(int i=0; i<characters::playable.size(); ++i) {
				if (characters::playable[i]->getHealth() <= 0) {
					characters::playable[i]->setMoved(false);
					characters::playable[i]->setActed(false);
				}
				characters::playable[i]->heal(characters::playable[i]->getMaxHealth());
			}
		}
		if(charge & 0x04) { // spirit
			for(int i=0; i<characters::playable.size(); ++i) {
				characters::playable[i]->setSpirit(characters::playable[i]->getMaxSpirit());
				display::sparkleSpiritMeter(characters::playable[i]);
			}
		}
		if(currentRoom->dungeonId) { // make sure the depletion happens before the file is saved!
			currentRoom->checkpoints[mostRecentCheckpoint].charge &= ~0x06;
			display::removeAnimationAtTile(currentRoom->checkpoints[mostRecentCheckpoint].x, currentRoom->checkpoints[mostRecentCheckpoint].y);
		}
		if(charge & 0x01) { // save 
			savefile::save(saveFile);
		}
	}

	int countShopItems() {
		return currentRoom->shopItems.size();
	}

	coinType getShopCurrency(int index) {
		if (index >= currentRoom->shopItems.size()) return (coinType) 0;
		return currentRoom->shopItems[index].currency;
	}

	int getShopPrice(int index) {
		if (index >= currentRoom->shopItems.size()) return 0;
		return currentRoom->shopItems[index].price;
	}

	ItemOrWeapon* getShopItem(int index) {
		if (index >= currentRoom->shopItems.size()) return 0;
		return currentRoom->shopItems[index].item;
	}

	int getShopQuantity(int index) {
		if (index >= currentRoom->shopItems.size()) return 0;
		return currentRoom->shopItems[index].maxStock;
	}

	void decrementShopSupply(int index) {
		if (currentRoom->shopItems[index].maxStock < 0) return;
		currentRoom->shopItems[index].maxStock -= 1;
		if (currentRoom->shopItems[index].maxStock == 0) {
			currentRoom->shopItems.erase(currentRoom->shopItems.begin() + index);
		}
	}

	void removeNpc(int npcId) {
		for(auto itr = npcs.begin(); itr != npcs.end(); ++itr) {
			if((*itr)->getId() == npcId) {
				npcs.erase(itr);
				return;
			}
		}
	}
	
	void npcWalk(int npcId, direction dir, int steps) {
		std::shared_ptr<NPC> npc;
		for(int i=0; i<npcs.size(); ++i) {
			if(npcs[i]->getId() == npcId) {
				npc = npcs[i];
			}
		}
		if(npc == NULL) return;
		
		npc->takeSteps(dir, steps);
	}
}
