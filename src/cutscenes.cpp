#include "characters.h"
#include "cutscenes.h"
#include "display.h"
#include "triggers.h"
#include "buttons.h"
#include "music.h"
#include "timekeeping.h"

#include <vector>
#include <fstream>
#include <sstream>

#include <iostream>

#include <random>

//forward declaration
namespace world {
	bool hasRoomFlag(int);
	void setRoomFlags(int);
	
	bool hasGlobalFlag(int);
	void addGlobalFlag(int);
	void removeGlobalFlag(int);
	
	void handleCheckpoint(int);
	void putItem(int, int, ItemOrWeapon*);

	void addNpc(int, int, int, int);
	void removeNpc(int);
	void npcWalk(int, direction, int);
	
	void loadMap(int);

	int getTileImage(int x, int y);
}

namespace display {
	void center(int, int);
}

namespace buttons {
	void hideTurnMenu();
}

#define NPC_WALK_CYCLE 4

namespace cutscenes {
	
	int responseCount;
	
	std::vector<cutsceneEvent> roomCutscene;
	std::vector<cutsceneEvent> fileCutscene;
	int itr;
	int waitFor = 0;
	
	std::vector<std::string> minorCharacterNames;
	std::vector<std::string> majorCharacterNames;
	std::vector<int> talkspriteOffsets;
	
	std::map<std::string, int> labels;
	
	int fullscreenR;
	int fullscreenG;
	int fullscreenB;
	
	bool _hasDialogue;
	bool canAdvance = true;

	bool fromFile = false;
	
	const std::vector<cutsceneEvent>* activeCutscene;
	
	std::vector<int> randomPool;
	
	void init() {
		std::ifstream majCharFile("misc/majchar.asdf");
		std::string line;
		std::stringstream lineStream;
		int talkspriteOffset = 0;
		while(getline(majCharFile, line)){
			lineStream.str(line);
			lineStream.clear();
			
			talkspriteOffsets.push_back(talkspriteOffset);
			
			std::string temp;
			std::getline(lineStream, temp, '\t');
			majorCharacterNames.push_back(temp);
			
			std::getline(lineStream, temp, '\t');
			talkspriteOffset += std::stoi(temp);
		}
		
		std::ifstream minCharFile("misc/minchar.asdf");
		while(getline(minCharFile, line)) minorCharacterNames.push_back(line);
		
		_hasDialogue = false;
	}
	
	void loadDialogue(const std::string& filename, std::vector<cutsceneEvent>& cutscene) {
		cutscene.clear();
		
		std::ifstream scriptFile(filename);
		if(!scriptFile.good()) {
			std::cout << "can't load file " << filename << " (this is fine, don't worry about it)" << std::endl;
			return;
		}
		
		std::string line;
		std::stringstream lineStream;
		
		int index = 0;
		
		while(getline(scriptFile, line)){
			lineStream.str(line);
			lineStream.clear();
			
			char eventType;
			lineStream.get(eventType);
			
			std::string temp;
			
			bool specialInit = false;
			int paramCount;
			
			cutsceneEvent event;
			event.type = eventType;
			
			if(eventType == ':') {
				std::getline(lineStream, temp);
				labels[temp] = index;
			}
			
			switch(eventType) {
				case 'd': { // dialogue
					// need to specify tabs because << stops at any whitespace and dialogue lines will have spaces
					std::getline(lineStream, temp, '\t');
					int minChar = std::stoi(temp);
					std::getline(lineStream, temp, '\t');
					int majChar = std::stoi(temp);
					
					std::string text;
					std::getline(lineStream, text, '\t');
					
					dialogueLine line = {minChar, majChar, text};
					event.details = line;
					
					specialInit = true;
					break;
				}
				
				case 'N': { // narration
					
					std::string text;
					std::getline(lineStream, text, '\t');
					
					dialogueLine line = {-2, -1, text};
					event.details = line;
					
					specialInit = true;
					break;
				}
				
				case 'b': { // branch
					
					std::getline(lineStream, temp, '\t');
					int minChar = std::stoi(temp);
					std::getline(lineStream, temp, '\t');
					int majChar = std::stoi(temp);
					
					std::string text;
					std::getline(lineStream, text, '\t');
					
					dialogueLine line = {minChar, majChar, text};
					
					std::getline(lineStream, temp, '\t');
					int count = std::stoi(temp);
					
					std::shared_ptr<std::vector<int>> indices { new std::vector<int>(count, 0) };
					std::shared_ptr<std::vector<std::string>> textOptions { new std::vector<std::string>(count, std::string()) };
					
					for(int i=0; i<count; ++i) {
						std::getline(lineStream, temp, '\t');
						(*indices)[i] = std::stoi(temp);
						std::getline(lineStream, (*textOptions)[i], '\t');
					}
					
					dialogueBranch branch = {count, line, indices, textOptions};
					event.details = branch;
					
					specialInit = true;
					break;
				}
				
				case 'e': // End cutscene
				case '?': // Random jump
					paramCount = 0; break;
				case 'f': // load cutscene File and continue
				case 'j': // unconditional Jump
				case 'p': // Parallel: execute next n instructions, then pause until all complete
				case '!': // Add to random pool

				case 's': // play Sound
				case 'm': // change Music
				case 'i': // display Image
				case 'C': // Checkpoint - save or heal
				case 'r': // Remove item from inventory
				case 'A': // Add room flag
				case 'a': // Add global flag
				case 'R': // Remove global flag
				case 'h': // Hide NPC
				case 'g': // Give item
				case 'w': // give Weapon
				case 'D': // door
				case 'S': // shop
					paramCount = 1; break;
				case 't': // activate Trigger on map
				case 'F': // branch on room Flag
				case 'G': // branch on Global flag
				case 'c': // center Camera on xy coordinates
				case 'I': // conditional on Item
				case '@': // party member joins
				case 'H': // Hide animation
				case 'M': // player character aniMation
				case '$': // give Coin
					paramCount = 2; break;
				case 'n': // npc walks
				case 'W': // conditional on coin.
							//stands for wallet or something idk i'm running out of letters
					paramCount = 3; break;
				case 'P': // Put npc
				case 'T': // branch on Terrain in room
					paramCount = 4; break;
				
				// a parallel command can be followed by at most one dialogue (d) line, and any number of other non-control-flow-affecting lines.
				// branch (b), end (e), file (f), jump (j), and parallel (p) are considered control-flow-affecting and are thus not elligible.
				// trigger (t) is elligible, but also instantaneous, so it doesn't make much sense
				// mostly it's going to be moving characters (w) and playing sounds (s) that gets parallelized
			}
			
			if(!specialInit) { // default initialization
				std::shared_ptr<std::vector<int>> params { new std::vector<int>(paramCount, 0) };
				for(int i=0; i<paramCount; ++i) {
					std::getline(lineStream, temp, '\t');
					(*params)[i] = std::stoi(temp);
				}
				event.details = params;
			}
			
			cutscene.push_back(event);
			++index;
		}
	}
	
	void loadRoomDialogue(int id) {
		loadDialogue("map/script/"+std::to_string(id), roomCutscene);
		if(!fromFile) {
			// if we're in the middle of a cutscene file, don't overwrite it with the new room
			activeCutscene = &roomCutscene;
		}
	}
	
	void loadDialogueFile(int id) {
		loadDialogue("script/"+std::to_string(id), fileCutscene);
		fromFile = true;
		activeCutscene = &fileCutscene;
	}
	
	const std::vector<cutsceneEvent> loadNpcItemDialogue(int id) {
		std::vector<cutsceneEvent> itemDialogue;
		loadDialogue("script/give/"+std::to_string(id), itemDialogue);
		return itemDialogue;
	}
	
	int getResponseCount() {
		return responseCount;
	}
	
	void startDialogueFromBranch(int index) {
		cutsceneEvent branchEvent = (fileCutscene.size() > 0) ? fileCutscene[itr] : roomCutscene[itr];
		itr = (*branchEvent.getBranch().indices)[index];
		advanceDialogue();
	}
	
	void startDialogue(int startIndex) {
		itr = startIndex;
		_hasDialogue = true;
		
		buttons::hideTurnMenu();
		
		advanceDialogue();
	}
	
	void startDialogueForItem(std::shared_ptr<NPC> npc, int item) {
		itr = npc->getGiveIndex(item);
		_hasDialogue = true;
		if (itr >= 0) {
			activeCutscene = &(npc->getGiveResponses());
			advanceDialogue();
		} else {
			loadDialogueFile(-2);
			startDialogue(0);
		}
	}
	
	void setAdvanceable() {
		canAdvance = true;
	}
	
	bool isAdvanceable() {
		return canAdvance;
	}
	
	void showDialogueLine(const dialogueLine& line) {
		std::string name = "";
		int talksprite = -1;
		if(line.majorCharacter >= 0) {
			talksprite = talkspriteOffsets[line.majorCharacter] + line.minorCharacter;
			name = majorCharacterNames[line.majorCharacter];
		} else if(line.majorCharacter == -3) {
			// TODO in the future, minorCharacter can be the index of a background animation here
			talksprite = -3;
			fullscreenR = (line.minorCharacter >> 16) & 0xFF;
			fullscreenG = (line.minorCharacter >> 8) & 0xFF;
			fullscreenB = line.minorCharacter & 0xFF;
		} else if(line.minorCharacter >= 0){
			name = minorCharacterNames[line.minorCharacter];
		}
		display::showDialogue(line.text, name, talksprite, line.majorCharacter);
	}
	
	void _advanceDialogue() {
		canAdvance = true;
		
		if(waitFor) {
			--waitFor;
			return;
		}
		
		responseCount = 0;
        
        const cutsceneEvent& ce = (*activeCutscene)[itr];
        
		if(!_hasDialogue || itr >= activeCutscene->size() || ce.type == 'e') {
			_hasDialogue = false;
			fromFile = false;
			display::hideDialogue();
			if(fileCutscene.size() > 0) fileCutscene.clear();
			activeCutscene = &roomCutscene; // set this back by default, if we just finished something other than a room cutscene
			buttons::showMainTurnMenu();
			display::center(characters::playable[buttons::getSelectedCharacter()]);
		}
		else if (ce.type == 'N' || ce.type == 'd') {
	
			canAdvance = false;
			showDialogueLine(ce.getLine());

			++itr;
			return;

		} else if (ce.type == 'b') {
			const dialogueBranch& branch = ce.getBranch();
			showDialogueLine(branch.prompt);
			
			buttons::showDialogueOptionButtons(branch.count);
			
			display::showDialogueOptions(branch.textOptions);
			responseCount = branch.count;
			return;
		} else {
			
			const std::vector<int>& p = *ce.getParams();
			
			switch(ce.type) {
				
				// Control flow
				
				case 'f': {
					loadDialogueFile(p[0]);
					startDialogue(0);
					return;
				}
				case 'j': {
					startDialogue(p[0]);
					return;
				}
				case 'p': {
					int parallels = p[0];
					for(int i=0; i<parallels; ++i) advanceDialogue();
					waitFor = parallels;
					return;
				}
				case 'F': {
					if(world::hasRoomFlag(p[0])) {
						startDialogue(p[1]);
						return;
					} else break;
				}
				case 'G': {
					if(world::hasGlobalFlag(p[0])) {
						startDialogue(p[1]);
						return;
					} else break;
				}
				case 'I': {
					if(characters::anyCharHasItem(p[0])) {
						startDialogue(p[1]);
						return;
					} else break;
				}
				case 'W': {
					if(characters::getCoinCount((coinType)p[0]) >= p[1]) {
						startDialogue(p[2]);
						return;
					} else break;
				}
				case 'T': {
					if(world::getTileImage(p[0],p[1]) == p[2]) {
						startDialogue(p[3]);
						return;
					} else break;
				}
				case 't': {
					triggers::triggerMeta(p[0],p[1]);
					break;
				}
				case '!': {
					randomPool.push_back(p[0]);
					break;
				}
				case '?': {
					startDialogue(randomPool[rand() % randomPool.size()]);
					randomPool.clear();
					return;
				}
				
				// Other
				
				case 'm': {
					music::switchTo(p[0]);
					break;
				}
				case 's': {
					music::playSound(std::to_string(p[0]));
					break;
				}
				case 'c': {
					display::center(p[0],p[1]);
					break;
				}
				case 'i': {
					display::showSceneImage(p[0]);
					break;
				}
				case 'C': {
					world::handleCheckpoint(p[0]);
					break;
				}
				case 'r': {
					characters::removeOneItem(p[0]);
					break;
				}
				case 'g': {
					item* _item = items::getItem(p[0]);
					PlayerCharacter* pc = characters::playable[buttons::getSelectedCharacter()];
					if(!pc->takeItem(_item)) {
						world::putItem(pc->getX(), pc->getY(), _item);
					}
					break;
				}
				case 'w': {
					weapon* _weapon = weapons::getWeapon(p[0]);
					PlayerCharacter* pc = characters::playable[buttons::getSelectedCharacter()];
					if(!pc->takeWeapon(_weapon)) {
						world::putItem(pc->getX(), pc->getY(), _weapon);
					}
					break;
				}
				case 'A': {
					world::setRoomFlags(p[0]);
					break;
				}
				case '@': {
					characters::addNextPlayable(p[0], p[1]);
					break;
				}
				case 'a': {
					world::addGlobalFlag(p[0]);
					break;
				}
				case 'R': {
					world::removeGlobalFlag(p[0]);
					break;
				}
				case 'h': {
					world::removeNpc(p[0]);
					break;
				}
				case 'H': {
					display::removeAnimationAtTile(p[0], p[1], false);
					// this will only be able to remove background animations, not foreground.
					break;
				}
				case 'n': {
					canAdvance = false;
					world::npcWalk(p[0], (direction)p[1], p[2]);
					timekeeping::schedule([](){cutscenes::advanceDialogue();}, FRAME_DURATION_DEFAULT * NPC_WALK_CYCLE * p[2]);
					++itr;
					return;
				}
				case 'P': {
					world::addNpc(p[0], p[1], p[2], p[3]);
					break;
				}
				case 'M': {
					characters::playable[p[0]]->setAnimationByIndex(p[1]);
					break;
				}
				case 'D': {
					world::loadMap(p[0]);
					break;
				}
				case 'S': {
					display::hideDialogue();
					_hasDialogue = false;
					canAdvance = true;
					fromFile = false;
					buttons::showShop(/*p[0]*/);
					return;
				}
				case '$': {
					characters::adjustCoins((coinType)(p[1]), p[0]);
					break;
				}
			}
			
			++itr;
			_advanceDialogue();
		}
	}
	
	void advanceDialogue() {
		if(!canAdvance) return;
		
		_advanceDialogue();
	}
	
	void cancelDialogue() {
		_hasDialogue = false;
		canAdvance = true;
		fromFile = false;
		fileCutscene.clear();
		display::hideDialogue();
	}
	
	bool hasDialogue() {
		return _hasDialogue;
	}
	
	int getFullscreenR() {
		return fullscreenR;
	}
	int getFullscreenG() {
		return fullscreenG;
	}
	int getFullscreenB() {
		return fullscreenB;
	}
	
}
