#include "characters.h"
#include "buttons.h"
#include "display.h"
#include "world.h"
#include "music.h"
#include "savefile.h"

#include <iostream>

namespace cutscenes {
	void startDialogueFromBranch(int);
	void startDialogueForItem(std::shared_ptr<NPC>, int);
	void cancelDialogue();
	void advanceDialogue();
	int getResponseCount();
}

namespace buttons {
	bool gameOpen = true;
	
	bool paused = false; // start false just in case we want animations on the title screen
	bool animationBlocking = false;

	std::vector<sf::Texture*> buttonTextures;
	
	std::map<std::string, sf::Texture*> textureCache; // needs to be in this file because of stupid initialization order nonsense
	
	int uiThemeCount;
	
	bool manualTravelMode = true;

	enum targetMode { noTarget=0, move, attack, castSpell, throwItem, giveItem, look };
	enum itemMode { noItems=0, inventory, ground, shop };
	
	targetMode target_mode;
	itemMode item_mode;
	
	int targetX;
	int targetY;
	
	int groundScrollOffset;
	
	bool travelMode = true;
	
	bool weaponCommitButtonsShowing = false;
	
	Button t_load_b (
		sf::IntRect(0x00,0x80,0x40,0x40),
		sf::IntRect(0x40,0x80,0x40,0x40),
		sf::IntRect(0x80,0x80,0x40,0x40),
		sf::IntRect(0xC0,0x80,0x40,0x40),
		(TOTALWIDTH-256)/2 + 0x44, 176,
		"startbuttons", true
	);
	Button t_new_b (
		sf::IntRect(0x00,0xC0,0x40,0x40),
		sf::IntRect(0x40,0xC0,0x40,0x40),
		sf::IntRect(0x80,0xC0,0x40,0x40),
		sf::IntRect(0xC0,0xC0,0x40,0x40),
		(TOTALWIDTH-256)/2, 176,
		"startbuttons", true
	);
	Button t_controls_b (5,0,5*MENU_BUTTON_SIZE, SCREENHEIGHT);
	
	Button t_exit_b (
		sf::IntRect(0,0x00,0x100,0x20),
		sf::IntRect(0,0x20,0x100,0x20),
		sf::IntRect(0,0x40,0x100,0x20),
		sf::IntRect(0,0x60,0x100,0x20),
		(TOTALWIDTH-256)/2, TOTALHEIGHT - MENU_BUTTON_SIZE,
		"startbuttons", true
	);
	
	Button t_settings_b (
		sf::IntRect(0,0x00,0x100,0x20),
		sf::IntRect(0,0x20,0x100,0x20),
		sf::IntRect(0,0x40,0x100,0x20),
		sf::IntRect(0,0x60,0x100,0x20),
		(TOTALWIDTH-256)/2, TOTALHEIGHT - MENU_BUTTON_SIZE * 2,
		"startbuttons", true
	);

	Button pause_b(
		sf::IntRect(0x00,0x100,0x40,0x40),
		sf::IntRect(0x40,0x100,0x40,0x40),
		sf::IntRect(0x80,0x100,0x40,0x40),
		sf::IntRect(0xC0,0x100,0x40,0x40),
		SCREENWIDTH + MENU_BUTTON_SIZE, SCREENHEIGHT - MENU_BUTTON_SIZE,
		"startbuttons", true
	);
	
	Button attack_b (0,0,0,SCREENHEIGHT);
	Button move_b (1,0,MENU_BUTTON_SIZE,SCREENHEIGHT);
	Button spirit_b (2,0,2*MENU_BUTTON_SIZE, SCREENHEIGHT);
	Button item_b (3,0,3*MENU_BUTTON_SIZE, SCREENHEIGHT);
	Button look_b (4,0,4*MENU_BUTTON_SIZE, SCREENHEIGHT);
	Button status_b (5,0,5*MENU_BUTTON_SIZE, SCREENHEIGHT);
	Button wait_b (6,0,SCREENWIDTH, SCREENHEIGHT);
	
	Button menu_cancel_b (5,1,7*MENU_BUTTON_SIZE, SCREENHEIGHT);
	
	Button return_b (9,1,6*MENU_BUTTON_SIZE, SCREENHEIGHT);
	
	Button i_use_b (0,1,264,408);
	Button i_equip_b(7,1,264,408);
	Button i_take_b (4,1,264,408);
	Button i_buy_b (10,0,264,408);
	Button i_drop_b (1,1,264 + 2 + MENU_BUTTON_SIZE, 408);
	Button i_throw_b (2,1,264 + 2*(2+MENU_BUTTON_SIZE), 408);
	Button i_give_b (3,1,264 + 3*(2+MENU_BUTTON_SIZE), 408);
	Button i_remove_b (7,0,264,408);
	
	Button travel_on_b(8,0,SCREENWIDTH, SCREENHEIGHT-2*MENU_BUTTON_SIZE);
	Button travel_off_b(8,1,SCREENWIDTH, SCREENHEIGHT-2*MENU_BUTTON_SIZE);
	
	Button respawn_b (9,0, (SCREENWIDTH-MENU_BUTTON_SIZE)/2, SCREENHEIGHT/2+MENU_BUTTON_SIZE);
	
	std::vector<std::vector<Button*> > targetButtons;
	std::map<int, Button*> largeTargetButtons;
	
	std::vector<Button*> pcButtons;

	std::unordered_set<Button*> menuVisibleButtons;
	std::unordered_set<Button*> gameVisibleButtons;
	
	std::vector<Button*> saveFileButtons;
	std::vector<Button*> itemButtons;
	std::vector<Button*> weaponButtons;
	std::vector<Button*> equipButtons;
	std::vector<Button*> groundItemButtons;
	std::vector<Button*> spellButtons;
	std::vector<Button*> dialogueButtons;
	std::vector<Button*> weaponCommitButtons;
	
	const sf::IntRect TARGET_BUTTON_DEFAULT(0, 0, TILESIZE, TILESIZE);
	const sf::IntRect TARGET_BUTTON_HIGHLIGHT(0, TILESIZE, TILESIZE, TILESIZE);
	const sf::IntRect TARGET_BUTTON_PRESSED(0, TILESIZE * 2, TILESIZE, TILESIZE);
	
	const sf::IntRect INVENTORY_BUTTON_DEFAULT(TILESIZE, 0, ITEM_IMGSIZE, ITEM_IMGSIZE);
	const sf::IntRect INVENTORY_BUTTON_HIGHLIGHT(TILESIZE, ITEM_IMGSIZE, ITEM_IMGSIZE, ITEM_IMGSIZE);
	const sf::IntRect INVENTORY_BUTTON_PRESSED(TILESIZE, ITEM_IMGSIZE*2, ITEM_IMGSIZE, ITEM_IMGSIZE);
	const sf::IntRect INVENTORY_BUTTON_DISABLED(TILESIZE, ITEM_IMGSIZE*3, ITEM_IMGSIZE, ITEM_IMGSIZE);
	
	const sf::IntRect WEAPON_BUTTON_DEFAULT(TILESIZE+ITEM_IMGSIZE,0, ITEM_IMGSIZE, ITEM_IMGSIZE);
	const sf::IntRect WEAPON_BUTTON_HIGHLIGHT(TILESIZE+ITEM_IMGSIZE, ITEM_IMGSIZE, ITEM_IMGSIZE, ITEM_IMGSIZE);
	const sf::IntRect WEAPON_BUTTON_PRESSED(TILESIZE+ITEM_IMGSIZE, ITEM_IMGSIZE*2, ITEM_IMGSIZE, ITEM_IMGSIZE);
	
	Button activeWeaponButton(
			WEAPON_BUTTON_DEFAULT,
			WEAPON_BUTTON_HIGHLIGHT,
			WEAPON_BUTTON_PRESSED,
			INVENTORY_BUTTON_DISABLED,
			WEAPONSPACE*ITEM_IMGSIZE + ITEM_IMGSIZE/2, ITEM_IMGSIZE*13/2,
			"select", false
	);
	
	Button groundScrollUp(
			sf::IntRect(MENU_BUTTON_SIZE*6, MENU_BUTTON_SIZE*4, MENU_BUTTON_SIZE, MENU_BUTTON_SIZE/2),
			sf::IntRect(MENU_BUTTON_SIZE*6, MENU_BUTTON_SIZE/2*9, MENU_BUTTON_SIZE, MENU_BUTTON_SIZE/2),
			sf::IntRect(MENU_BUTTON_SIZE*6, MENU_BUTTON_SIZE*5, MENU_BUTTON_SIZE, MENU_BUTTON_SIZE/2),
			sf::IntRect(MENU_BUTTON_SIZE*6, MENU_BUTTON_SIZE/2*11, MENU_BUTTON_SIZE, MENU_BUTTON_SIZE/2),
			ITEM_IMGSIZE*21/2, ITEM_IMGSIZE/2,
			"buttons", true
	);
	
	Button groundScrollDown(
			sf::IntRect(MENU_BUTTON_SIZE*6, MENU_BUTTON_SIZE*6, MENU_BUTTON_SIZE, MENU_BUTTON_SIZE/2),
			sf::IntRect(MENU_BUTTON_SIZE*6, MENU_BUTTON_SIZE/2*13, MENU_BUTTON_SIZE, MENU_BUTTON_SIZE/2),
			sf::IntRect(MENU_BUTTON_SIZE*6, MENU_BUTTON_SIZE*7, MENU_BUTTON_SIZE, MENU_BUTTON_SIZE/2),
			sf::IntRect(MENU_BUTTON_SIZE*6, MENU_BUTTON_SIZE/2*15, MENU_BUTTON_SIZE, MENU_BUTTON_SIZE/2),
			ITEM_IMGSIZE*21/2, ITEM_IMGSIZE/2 * (3 + GROUNDSPACE*3),
			"buttons", true
	);


	Button m_unpause_b (
			sf::IntRect(0,0x00,0x100,0x20),
			sf::IntRect(0,0x20,0x100,0x20),
			sf::IntRect(0,0x40,0x100,0x20),
			sf::IntRect(0,0x60,0x100,0x20),
			4*ITEM_IMGSIZE + 19, ITEM_IMGSIZE/2 + 33,
			"startbuttons", true
	);
	
	Button m_music_up_b (
			sf::IntRect(0x120, 0x00, 0x20, 0x20),
			sf::IntRect(0x120, 0x20, 0x20, 0x20),
			sf::IntRect(0x120, 0x40, 0x20, 0x20),
			sf::IntRect(0x120, 0x60, 0x20, 0x20),
			4*ITEM_IMGSIZE + 263, ITEM_IMGSIZE/2 + 73,
			"startbuttons", true
	);
	Button m_music_down_b (
			sf::IntRect(0x100, 0x00, 0x20, 0x20),
			sf::IntRect(0x100, 0x20, 0x20, 0x20),
			sf::IntRect(0x100, 0x40, 0x20, 0x20),
			sf::IntRect(0x100, 0x60, 0x20, 0x20),
			4*ITEM_IMGSIZE + 183, ITEM_IMGSIZE/2 + 73,
			"startbuttons", true
	);
	
	Button m_sound_up_b (
			sf::IntRect(0x120, 0x00, 0x20, 0x20),
			sf::IntRect(0x120, 0x20, 0x20, 0x20),
			sf::IntRect(0x120, 0x40, 0x20, 0x20),
			sf::IntRect(0x120, 0x60, 0x20, 0x20),
			4*ITEM_IMGSIZE + 263, ITEM_IMGSIZE/2 + 111,
			"startbuttons", true
	);
	Button m_sound_down_b (
			sf::IntRect(0x100, 0x00, 0x20, 0x20),
			sf::IntRect(0x100, 0x20, 0x20, 0x20),
			sf::IntRect(0x100, 0x40, 0x20, 0x20),
			sf::IntRect(0x100, 0x60, 0x20, 0x20),
			4*ITEM_IMGSIZE + 183, ITEM_IMGSIZE/2 + 111,
			"startbuttons", true
	);
	
	Button m_theme_up_b (
			sf::IntRect(0x120, 0x00, 0x20, 0x20),
			sf::IntRect(0x120, 0x20, 0x20, 0x20),
			sf::IntRect(0x120, 0x40, 0x20, 0x20),
			sf::IntRect(0x120, 0x60, 0x20, 0x20),
			4*ITEM_IMGSIZE + 323, ITEM_IMGSIZE/2 + 187,
			"startbuttons", true
	);
	Button m_theme_down_b (
			sf::IntRect(0x100, 0x00, 0x20, 0x20),
			sf::IntRect(0x100, 0x20, 0x20, 0x20),
			sf::IntRect(0x100, 0x40, 0x20, 0x20),
			sf::IntRect(0x100, 0x60, 0x20, 0x20),
			4*ITEM_IMGSIZE + 183, ITEM_IMGSIZE/2 + 187,
			"startbuttons", true
	);
	
	Button m_fullscreen_b (
		sf::IntRect(0,0x00,0x100,0x20),
		sf::IntRect(0,0x20,0x100,0x20),
		sf::IntRect(0,0x40,0x100,0x20),
		sf::IntRect(0,0x60,0x100,0x20),
		4*ITEM_IMGSIZE + 19, ITEM_IMGSIZE/2 + 147,
		"startbuttons", true
	);
	
	Button m_exit_b (
		sf::IntRect(0,0x00,0x100,0x20),
		sf::IntRect(0,0x20,0x100,0x20),
		sf::IntRect(0,0x40,0x100,0x20),
		sf::IntRect(0,0x60,0x100,0x20),
		4*ITEM_IMGSIZE + 19, ITEM_IMGSIZE/2 + 261,
		"startbuttons", true
	);
	
	
	
	std::vector<bool> existingSaveFiles;

	int selectedCharacter = -1;
	int selectedItem = -1;
	
	int selectedMenuItem = -1;
	
	int getSelectedCharacter(){return selectedCharacter;}
	int getSelectedItem(){return selectedItem;}
	int getSelectedMenuItem(){return selectedMenuItem;}
	int getGroundScrollOffset(){return groundScrollOffset;}
	
	int before_cancel_state = 0;
#define STATE_NO_CHAR 0
#define STATE_CHAR_MENU 1
#define STATE_INVENTORY 2
#define STATE_GROUND_ITEMS 3
#define STATE_TARGET_MOVE 4
#define STATE_TARGET_WEAPON 5
#define STATE_TARGET_SPELL 6
#define STATE_TARGET_THROW 7
	
	void init(){

//		wait_b = Button();
		selectionsTexture.loadFromFile("img/select.png");
			
		t_new_b.effect      = buttons::onNewGame;
		t_load_b.effect     = buttons::onLoadGame;
		t_settings_b.effect = buttons::onPause;
		t_exit_b.effect     = buttons::onExitGame;
		
		pause_b.effect = buttons::onPause;
		
		m_unpause_b.effect    = buttons::onUnpause;
		m_exit_b.effect       = buttons::showTitleScreen;
		m_music_up_b.effect   = [](){buttons::onAdjustMusic(1);    };
		m_music_down_b.effect = [](){buttons::onAdjustMusic(-1);   };
		m_sound_up_b.effect   = [](){buttons::onAdjustSound(1);    };
		m_sound_down_b.effect = [](){buttons::onAdjustSound(-1);   };
		m_theme_up_b.effect   = [](){buttons::onAdjustUiTheme(1);  };
		m_theme_down_b.effect = [](){buttons::onAdjustUiTheme(-1); };
		m_fullscreen_b.effect = buttons::onToggleFullscreen;

		move_b.effect   = buttons::onMove;
		attack_b.effect = buttons::onAttack;
		spirit_b.effect = buttons::onSpirit;
		item_b.effect   = buttons::onItem;
		wait_b.effect   = buttons::onEndTurn;
		status_b.effect = buttons::onStatus;
		look_b.effect   = buttons::onLook;
		return_b.effect = buttons::onReturn;
		menu_cancel_b.effect = buttons::onCancel;
		
		i_use_b.effect    = buttons::onItemUse;
		i_take_b.effect   = buttons::onItemTake;
		i_buy_b.effect    = buttons::onItemBuy;
		i_drop_b.effect   = buttons::onItemDrop;
		i_throw_b.effect  = buttons::onItemThrow;
		i_give_b.effect   = buttons::onItemGive;
		i_equip_b.effect  = buttons::onItemEquip;
		i_remove_b.effect = buttons::onItemRemove;
		
		travel_on_b.effect  = [](){buttons::onTravelModeButton(true); };
		travel_off_b.effect = [](){buttons::onTravelModeButton(false);};
		
		respawn_b.effect = buttons::onRespawn;
		
		activeWeaponButton.effect = [](){buttons::selectWeapon(WEAPONSPACE);};
		
		groundScrollUp.effect = [](){buttons::incrementGroundScroll(-1);};
		groundScrollDown.effect = [](){buttons::incrementGroundScroll(1);};

		for(int i=0; i<MAX_RANGE*2 + 1; ++i){
			targetButtons.push_back(std::vector<Button*>());
			for(int j=0; j<MAX_RANGE*2 + 1; ++j){
				Button* b = new Button(
					TARGET_BUTTON_DEFAULT,
					TARGET_BUTTON_HIGHLIGHT,
					TARGET_BUTTON_PRESSED,
					TARGET_BUTTON_DEFAULT,
					(SCREENWIDTH - TILESIZE)/2 + (i - MAX_RANGE)*TILESIZE,
					(SCREENHEIGHT - TILESIZE)/2 + (j - MAX_RANGE)*TILESIZE,
					"select", false
				);
				b->effect = [i,j](){buttons::target(i-MAX_RANGE,j-MAX_RANGE);};
				targetButtons[i].push_back(b);
			}
		}
		
		for(int i=0; i<ITEMSPACE; ++i){ // TODO max post-modification item space
			Button* b = new Button(
				INVENTORY_BUTTON_DEFAULT,
				INVENTORY_BUTTON_HIGHLIGHT,
				INVENTORY_BUTTON_PRESSED,
				INVENTORY_BUTTON_DISABLED,
				(i%INVENTORY_ROW)*ITEM_IMGSIZE + ITEM_IMGSIZE/2, (i/INVENTORY_ROW + 2)*ITEM_IMGSIZE,
				"select", false
			);
			b->effect = [i](){buttons::selectItem(i);};
			itemButtons.push_back(b);
		}
		for(int i=0; i<EQUIPSPACE; ++i){
			Button* b = new Button(
				INVENTORY_BUTTON_DEFAULT,
				INVENTORY_BUTTON_HIGHLIGHT,
				INVENTORY_BUTTON_PRESSED,
				INVENTORY_BUTTON_DISABLED,
				i*ITEM_IMGSIZE + ITEM_IMGSIZE/2, ITEM_IMGSIZE/2,
				"select", false
			);
			b->effect = [i](){buttons::selectEquipment(i);};
			equipButtons.push_back(b);
		}
		for(int i=0; i<WEAPONSPACE; ++i){
			Button* b = new Button(
				INVENTORY_BUTTON_DEFAULT,
				WEAPON_BUTTON_HIGHLIGHT,
				WEAPON_BUTTON_PRESSED,
				INVENTORY_BUTTON_DISABLED,
				i*ITEM_IMGSIZE + ITEM_IMGSIZE/2, ITEM_IMGSIZE*13/2,
				"select", false
			);
			b->effect = [i](){buttons::selectWeapon(i);};
			weaponButtons.push_back(b);
		}
		for(int i=0; i<GROUNDSPACE; ++i){
			Button* b = new Button(
				INVENTORY_BUTTON_DEFAULT,
				INVENTORY_BUTTON_HIGHLIGHT,
				INVENTORY_BUTTON_PRESSED,
				INVENTORY_BUTTON_DISABLED,
				ITEM_IMGSIZE*21/2, ITEM_IMGSIZE/2*(3 + i*3),
				"select", false
			);
			b->effect = [i](){buttons::selectGroundItem(i);};
			groundItemButtons.push_back(b);
		}
		
		for(int i=0; i<SPELLSPACE; ++i){
			Button* b = new Button(
				sf::IntRect(0,0   ,190,42),
				sf::IntRect(0,42  ,190,42),
				sf::IntRect(0,42*2,190,42),
				sf::IntRect(0,42*3,190,42),
				0, SCREENHEIGHT + 43 * (i-3),
				"spellMenuButtons", true
			);
			b->effect = [i](){buttons::selectSpell(i);};
			spellButtons.push_back(b);
		}
		for(int i=0; i<MAX_COMMIT; ++i){
			Button* b = new Button(
				sf::IntRect(64*i,0   ,64,64),
				sf::IntRect(64*i,64  ,64,64),
				sf::IntRect(64*i,64*2,64,64),
				sf::IntRect(64*i,64*3,64,64),
				64*i, SCREENHEIGHT-64,
				"weaponCommitButtons", true
			);
			b->effect = [i](){buttons::onCommitAttack(i);};
			weaponCommitButtons.push_back(b);
		}
		for(int i=0; i<4; ++i){
			Button* b = new Button(
				sf::IntRect(0,0,575,20),
				sf::IntRect(0,20,575,20),
				sf::IntRect(0,20*2,575,20),
				sf::IntRect(0,20*3,575,20),
				118, SCREENHEIGHT-119+20*i,
				"dialogueButtons", true
			);
			b->effect = [i](){
				buttons::hideDialogueOptionButtons();
				cutscenes::startDialogueFromBranch(i);
			};
			dialogueButtons.push_back(b);
		}
		
		

		for(int i=0; i<4; ++i){
			Button* b = new Button(
				sf::IntRect(0,0x00,0x100,0x20),
				sf::IntRect(0,0x20,0x100,0x20),
				sf::IntRect(0,0x40,0x100,0x20),
				sf::IntRect(0,0x60,0x100,0x20),
				(TOTALWIDTH-256)/2, 244 + 36*i,
				"startbuttons", true
			);
			b->effect = [i](){buttons::selectSaveFile(i);};
			saveFileButtons.push_back(b);
		}
		
		setVisible(&wait_b, true);
		setVisible(&travel_off_b, characters::playable.size() > 1);
		
		setVisible(&pause_b, true);
	}
	
	void initPartyButtons() {
		for(int i=0; i<pcButtons.size(); ++i) {
			setVisible(pcButtons[i], false);
			delete pcButtons[i];
		}
		pcButtons.clear();
		
		for(int i=0; i<characters::playable.size(); ++i){
			Button* pcbutton = new Button(
				sf::IntRect(128*i,0,128,128),
				sf::IntRect(128*i,128,128,128),
				sf::IntRect(128*i,128,128,128),
				sf::IntRect(128*i,256,128,128),
				SCREENWIDTH, 128*i,
				"pccards", false
			);
			pcbutton->effect = [i](){buttons::selectCharacter(i);};
			pcButtons.push_back(pcbutton);
			setVisible(pcbutton, true);
		}
	}
	
	void clearMenuButtons() {
		for(Button* b : menuVisibleButtons) b->reset();
		menuVisibleButtons.clear();
	}
	
	/*void clearButtons() {
		for(Button* b : gameVisibleButtons) b->reset();
		gameVisibleButtons.clear();
	}*/
	
	void showTitleScreen() {
		selectedMenuItem = -1;
		paused = false;
		
		clearMenuButtons();
		hideInventory();
		cutscenes::cancelDialogue();
		
		music::switchTo(0);
		
		for(Button* b : saveFileButtons) {
			setMenuVisible(b, true);
			existingSaveFiles.push_back(savefile::exists(existingSaveFiles.size()));
		}
		
		//setMenuVisible(&t_controls_b, true);
		setMenuVisible(&t_exit_b, true);
		setMenuVisible(&t_settings_b, true);

		display::showStartMenu();

		world::clearRoomCache();
	}
	
	bool saveFileExists(int id) {
		return existingSaveFiles[id];
	}
	
	void selectSaveFile(int saveId) {
		selectedItem = saveId;
		
		setMenuVisible(&t_new_b, true);
		setMenuVisible(&t_load_b, true);
		
		t_load_b.disable(!saveFileExists(saveId));
	}
	
	void onNewGame() {
		savefile::clear(selectedItem);
		existingSaveFiles[selectedItem] = true;
		
		for(int i=0; i<pcButtons.size(); ++i) {
			setVisible(pcButtons[i], false);
			delete pcButtons[i];
		}
		pcButtons.clear();
		
		characters::loadNew(); // this calls addNextCharacter, which creates the new pcbutton
		selectedCharacter = 0;
		
		world::loadNew(selectedItem);
		display::center(characters::playable[0]);
		
		display::clearView();
		paused = false;
	}
	
	void onLoadGame() {
		savefile::load(selectedItem);
		initPartyButtons();
		selectCharacter(0);
		display::clearView();
		paused = false;
	}
	
	void onExitGame() {
		gameOpen = false;
		world::cleanup();
		selectedCharacter = -1;
	}
	
	bool isPaused() {
		return paused;
	}
	
	void onPause() {
		selectedMenuItem = 0;
		paused = true;
		clearMenuButtons();
		setMenuVisible(&m_unpause_b, !display::isStartMenu());
		setMenuVisible(&m_exit_b, true);
		
		setMenuVisible(&m_music_up_b, true);
		setMenuVisible(&m_music_down_b, true);
		setMenuVisible(&m_sound_up_b, true);
		setMenuVisible(&m_sound_down_b, true);
		setMenuVisible(&m_theme_up_b, true);
		setMenuVisible(&m_theme_down_b, true);
		setMenuVisible(&m_fullscreen_b, true);
	}
	
	void onUnpause() {
		selectedMenuItem = -1;
		paused = false;
	}
	
	void onAdjustMusic(int delta) {
		music::setMusicVolume(music::getMusicVolume() + delta);
		m_music_up_b.disable(music::getMusicVolume() >= MAX_VOLUME);
		m_music_down_b.disable(music::getMusicVolume() <= 0);
	}
	
	void onAdjustSound(int delta) {
		music::setSoundVolume(music::getSoundVolume() + delta);
		m_sound_up_b.disable(music::getSoundVolume() >= MAX_VOLUME);
		m_sound_down_b.disable(music::getSoundVolume() <= 0);
	}
	
	void onAdjustUiTheme(int delta) {
		setUiTheme((getUiTheme() + uiThemeCount + delta) % uiThemeCount);
		display::setUiTheme(getUiTheme());
	}
	
	void setThemeCount(int tc) {
		uiThemeCount = tc;
	}

	void onToggleFullscreen() {
		// do later
	}

	
	
	void setTargetResult(targetMode tm){target_mode = tm;}

	bool isActive(Button* b){
		if (paused || display::isStartMenu()) {
			return menuVisibleButtons.find(b) != menuVisibleButtons.end() && ! b->isDisabled();
		} else {
			return gameVisibleButtons.find(b) != gameVisibleButtons.end() && ! b->isDisabled();
		}
	}
	
	void showMainTurnMenu() {
		hideTargets();
		hideInventory();
		before_cancel_state = STATE_CHAR_MENU;
		
		PlayerCharacter* pc = characters::playable[selectedCharacter];
		
		bool onscreen = (pc->getRoomId() == world::getRoomId());
		
		setVisible(&attack_b, onscreen);
		setVisible(&spirit_b, onscreen);
		setVisible(&item_b, onscreen);
		setVisible(&move_b, onscreen);
		setVisible(&status_b, true);
		setVisible(&look_b, onscreen);
		setVisible(&return_b, !onscreen);
		
		setVisible(&menu_cancel_b, false);
		
		setVisible(&wait_b, true);
		
		move_b.disable(pc->getHealth() > 0 && pc->stepsLeft() == 0);
		//wait_b.disable(pc->stepsLeft() == 0 && characters::playable[charId]->hasActed());
				
		if(pc->hasActed()){
			attack_b.disable(true);
			spirit_b.disable(true);
		} else {
			attack_b.disable(false);
			spirit_b.disable(false);
		}
	}
	
	void addPlayerCharacter() {
		int i = pcButtons.size();
		
		Button* pcbutton = new Button(
			sf::IntRect(128*i,0,128,128),
			sf::IntRect(128*i,128,128,128),
			sf::IntRect(128*i,128,128,128),
			sf::IntRect(128*i,256,128,128),
			SCREENWIDTH, 128*i,
			"pccards", false
		);
		pcbutton->effect = [i](){buttons::selectCharacter(i);};
		pcButtons.push_back(pcbutton);
		setVisible(pcbutton, true);
		if (i > 0) setVisible(travelMode ? &travel_off_b : &travel_on_b, true);
	}
	
	void refreshGroundItems() {
		
		PlayerCharacter* pc = characters::playable[selectedCharacter];
		int itemCount = world::countItemsAt(pc->getX(), pc->getY());
		
		setVisible(&groundScrollUp, true);
		setVisible(&groundScrollDown, true);
		groundScrollUp.disable(groundScrollOffset == 0);
		groundScrollDown.disable(groundScrollOffset + GROUNDSPACE >= itemCount);
		
		for(int i=0; i<GROUNDSPACE && i<itemCount; ++i) setVisible(groundItemButtons[i], true);
	}
	
	void refreshShopItems() {
		int itemCount = world::countShopItems();
	
		setVisible(&groundScrollUp, true);
		setVisible(&groundScrollDown, true);
		groundScrollUp.disable(groundScrollOffset == 0);
		groundScrollDown.disable(groundScrollOffset + GROUNDSPACE >= itemCount);
		
		for(int i=0; i<GROUNDSPACE && i<itemCount; ++i) setVisible(groundItemButtons[i], true);
	}

	void onPlayerMove(PlayerCharacter* pc){
		if(!display::hasDialogue() && pc->getHealth() > 0) {
			if(pc == characters::playable[selectedCharacter]) {
				showMainTurnMenu();
				if (world::countItemsAt(pc->getX(),pc->getY())) {
					before_cancel_state = STATE_CHAR_MENU; // used to be STATE_TARGET_MOVE, but moving with buttons is deprecated
					setVisible(&menu_cancel_b, true);
					
					item_mode = ground;
					
					selectedItem = -1;
					display::showGroundItems();
					groundScrollOffset = 0;
					
					refreshGroundItems();
				} else {
					display::clearView();
					hideGroundItems();
					item_mode = noItems;
					if(pc->stepsLeft() <= 0) { // not sure how this interacts with ghost characters
						move_b.disable(true);
					}
				}
			}
		}
	}
	
	void showWeaponCommitButtons() {
		for(int i=0; i<weaponCommitButtons.size(); ++i) {
			setVisible(weaponCommitButtons[i], true);
			weaponCommitButtons[i]->disable(characters::playable[selectedCharacter]->stepsLeft() < i);
		}
		selectedItem = -1;
		weaponCommitButtonsShowing = true;
	}
	
	bool hasWeaponCommitButtons() {
		return weaponCommitButtonsShowing;
	}

	void setTargetLocation(int x, int y) {
		targetX = x;
		targetY = y;
	}

	void target(int x, int y){
		if(!target_mode) return;
		if(x != 0 && y != 0 && !isActive(targetButtons[x+MAX_RANGE][y+MAX_RANGE])) return;
		
		PlayerCharacter* pc = characters::playable[selectedCharacter];
		
		// convert from screen coordinates to grid coordinates
		x+=pc->getX();
		y+=pc->getY();
		
		// cache target location in case we need to prompt more user input (e.g. weapon commits)
		setTargetLocation(x,y);
		
		switch(target_mode){
			case move:
				pc->moveAction(x,y);
				onPlayerMove(pc);
				if(pc->stepsLeft() > 0) {
					onMove();
				}
				return;
			case attack:
				hideTargets();
				showWeaponCommitButtons();
				return;
			case throwItem:
				setAnimationBlocking(true);
				pc->throwItem(selectedItem, x, y);
				return;
			case giveItem: {
				for(int i=0; i<characters::playable.size(); ++i){
					PlayerCharacter* otherPC = characters::playable[i];
					if(selectedItem >= SELECT_WEAPON){
						if(otherPC->getX() == x && otherPC->getY() == y && !otherPC->weaponsFull()){
							otherPC->takeWeapon(pc->popWeapon(selectedItem-SELECT_WEAPON));
							hideTargets();
							onItem();
							return;
						}
					} else {
						if(otherPC->getX() == x && otherPC->getY() == y && !otherPC->itemsFull()){
							otherPC->takeItem(pc->popItem(selectedItem));
							hideTargets();
							onItem();
							return;
						}
					}
				}
				const std::vector<std::shared_ptr<NPC>> npcs = world::getNPCs();
				for(int i=0;i<npcs.size();++i){
					int deltaX = pc->getX() - npcs[i]->getX();
					int deltaY = pc->getY() - npcs[i]->getY();
					if(deltaX * deltaX + deltaY * deltaY == 1){
						hideTargets();
						// Don't deduct the selected item; do it in the script file instead
						cutscenes::startDialogueForItem(npcs[i], pc->getItem(selectedItem));
						return;
					}
				}
				break;
			}
			case look:
				world::onLook(x,y);
				hideTargets();
				return;
			case castSpell:
				setAnimationBlocking(true);
				pc->castSpell(spells::get(selectedItem), x, y);
				return;
			default:
				return;
		}
	}

	void selectLook() {
		display::clearView();
		world::onLook(targetX,targetY);
	}

	void selectCharacter(int charId){
		if(travelMode && !world::isCombat()) {
			if (selectedCharacter >= 0) {
				PlayerCharacter* pc = characters::playable[selectedCharacter];
				int oldX = pc->getX();
				int oldY = pc->getY();
				pc->setXY(-1, -1); // move the other character out of the way
				
				selectedCharacter = charId;
				pc = characters::playable[selectedCharacter];
				display::center(pc);
				
				pc->setXY(oldX, oldY);
			} else {
				selectedCharacter = charId;
				display::center(characters::playable[selectedCharacter]);
			}
			selectedItem = -1;
			
			hideTargets();
			if (item_mode != noItems) {
				hideInventory();
				onItem();
			}
		} else {
			selectedCharacter = charId;
			PlayerCharacter* pc = characters::playable[charId];
			display::center(pc);
			pc->skip(false);
			showMainTurnMenu();
		}
		music::onCharacterSelect();
	}
	
	bool isNonInteractive() {
		return animationBlocking || !world::isPlayerTurn() || cutscenes::hasDialogue() || display::isDeathScreen();
	}
	
	void onNextCharacter() {
		if(isNonInteractive() || selectedCharacter < 0) return;
		toNextCharacter(true);
	}
	
	void toNextCharacter(bool unskip){
	
		if(travelMode && !world::isCombat()) {
			PlayerCharacter* pc = characters::playable[selectedCharacter];
			int oldX = pc->getX();
			int oldY = pc->getY();
			
			pc->setXY(-1, -1); // move the other character out of the way
			
			++selectedCharacter;
			if(selectedCharacter >= characters::playable.size()) selectedCharacter = 0;

			pc = characters::playable[selectedCharacter];
			
			pc->setXY(oldX, oldY);
			display::center(pc);
			selectedItem = -1;
			hideTargets();
			if (item_mode != noItems) {
				hideInventory();
				onItem();
			}
			
		} else for(int i=1; i<=characters::playable.size(); ++i){
			// if this cycled all the way around, it would reselect the current character,
			// which would potentially unskip them.
			PlayerCharacter* pc = characters::playable[(i+selectedCharacter)%characters::playable.size()];
			if( (unskip ? (!pc->hasActed() || pc->stepsLeft()>0) : !pc->isDone()) ){
				// that's a bad conditional.
				selectCharacter((i+selectedCharacter)%characters::playable.size());
				showMainTurnMenu();
				return;
			}
		}
	}
	
	void hideItemActions(){
		setVisible(&i_take_b, false);
		setVisible(&i_buy_b,  false);
		setVisible(&i_use_b,  false);
		setVisible(&i_drop_b, false);
		setVisible(&i_throw_b,false);
		setVisible(&i_give_b, false);
		setVisible(&i_equip_b,false);
		setVisible(&i_remove_b, false);
	}
	
	bool giveInRange() {
		PlayerCharacter* pc = characters::playable[selectedCharacter];
		for(int i=0;i<characters::playable.size();++i){
			int deltaX = pc->getX() - characters::playable[i]->getX();
			int deltaY = pc->getY() - characters::playable[i]->getY();
			if(deltaX * deltaX + deltaY * deltaY == 1 && !characters::playable[i]->itemsFull()){
				return true;
			}
		}
		const std::vector<std::shared_ptr<NPC>> npcs = world::getNPCs();
		for(int i=0;i<npcs.size();++i){
			int deltaX = pc->getX() - npcs[i]->getX();
			int deltaY = pc->getY() - npcs[i]->getY();
			if(deltaX * deltaX + deltaY * deltaY == 1){
				return true;
				break;
			}
		}
		return false;
	}
	
	void selectItem(int itemId){
		//setVisible(&use_b, true);
		//setVisible(&throw_b, true);
		//setVisible(&drop_b, true);
		
		PlayerCharacter* pc = characters::playable[selectedCharacter];
		
		selectedItem = itemId;
		
		item* it = pc->getInventory()[selectedItem];
		if (!it) return;
		
		bool isEquip = it && pc->getInventory()[selectedItem]->useEffect.effect == ieEquipment;
		setVisible(&i_equip_b, isEquip);
		setVisible(&i_use_b, !isEquip);
		setVisible(&i_drop_b, true);
		setVisible(&i_take_b, false);
		setVisible(&i_buy_b, false);
		setVisible(&i_throw_b, true);
		setVisible(&i_give_b, true);
		
		i_drop_b.disable(pc->getHealth() == 0 || !it);
		
		if(pc->hasActed()){
			i_equip_b.disable(true);
			i_use_b.disable(true);
			i_throw_b.disable(true);
		} else {
			i_equip_b.disable(!it || !pc->equipsFull());
			i_use_b.disable(!it || pc->getInventory()[selectedItem]->useEffect.effect == ieDefaultEffect);
			i_throw_b.disable(!it);
		}
		i_give_b.disable(!it || !giveInRange()); // unless another pc in range
	}
	
	void selectWeapon(int weaponId){
		
		selectedItem = weaponId + SELECT_WEAPON;

		setVisible(&i_use_b, false);
		setVisible(&i_throw_b, false);
		setVisible(&i_take_b, false);
		setVisible(&i_buy_b, false);

		if(weaponId < WEAPONSPACE) {
			PlayerCharacter* pc = characters::playable[selectedCharacter];
			
			weapon* w = pc->getOffhandWeapons()[weaponId];

			setVisible(&i_equip_b, true);
			setVisible(&i_drop_b, true);
			setVisible(&i_give_b, true);
			
			i_equip_b.disable(!w || pc->hasActed() || !pc->hasWeaponClass(w->wclass));
			i_drop_b.disable(pc->getHealth() == 0 || !w);
			i_give_b.disable(true); // unless another pc in range
			
			if(w) for(int i=0;i<characters::playable.size();++i){
				int deltaX = pc->getX() - characters::playable[i]->getX();
				int deltaY = pc->getY() - characters::playable[i]->getY();
				if(deltaX * deltaX + deltaY * deltaY == 1 && !characters::playable[i]->itemsFull()){
					i_give_b.disable(false);
					break;
				}
			}
		} else {
			setVisible(&i_equip_b, false);
			setVisible(&i_drop_b, false);
			setVisible(&i_give_b, false);
		}
	}
	
	void selectEquipment(int equipIndex){
		
		selectedItem = equipIndex + SELECT_EQUIPMENT;
		
		PlayerCharacter* pc = characters::playable[selectedCharacter];
		int equipmentId = pc->getEquipment()[equipIndex];

		hideItemActions();
		setVisible(&i_remove_b, equipmentId>=0);
		
		i_remove_b.disable(equipmentId < 0 || pc->itemsFull());

	}
	
	void selectGroundItem(int itemIndex) {
		PlayerCharacter* pc = characters::playable[selectedCharacter];
		
		selectedItem = itemIndex + SELECT_GROUND + groundScrollOffset;
		
		hideTurnMenu();
		hideItemActions();
		setVisible(&menu_cancel_b, true);

		if (item_mode == itemMode::shop) {
			setVisible(&i_buy_b, true);
			coinType itemCurrency = world::getShopCurrency(itemIndex);
			int itemPrice = world::getShopPrice(itemIndex);
			pickupableType ptype = world::getShopItem(itemIndex)->getPickupableType();
			int coinCount = characters::getCoinCount(itemCurrency);
			i_buy_b.disable(coinCount < itemPrice || (ptype == ptItem && pc->itemsFull()) || (ptype == ptWeapon && pc->weaponsFull()));
		} else {
			setVisible(&i_take_b, true);
			pickupableType ptype = world::getItemsAt(pc->getX(), pc->getY())[itemIndex]->getPickupableType();
			i_take_b.disable(pc->getHealth() <= 0 || (ptype == ptItem && pc->itemsFull()) || (ptype == ptWeapon && pc->weaponsFull()));
		}
	}
	
	void enableTargetsForSpell(std::shared_ptr<spell> s) {
		hideInventory();
	
		selectedItem = s->id;
		setTargetResult(targetMode::castSpell);
		
		if(s->rtype == range_type::self) {
			allowLargeTarget(spells::getAreaRadius(s, characters::playable[selectedCharacter]));
		} else {
			// hide any existing targets - otherwise, could look then use spell hotkey to do stupid things
			hideTargets();
			allowTargets(spells::getTargets(s, characters::playable[selectedCharacter]));
		}
		
		hideSpiritMenu();
		display::clearView();
		display::showSpellDescription();
	}
	
	void selectSpell(int spellIndex){
		
		PlayerCharacter* pc = characters::playable[selectedCharacter];
		
		if(pc->getRoomId() != world::getRoomId()) return; // offscreen characters can't cast spells
			
		std::shared_ptr<spell> s = pc->getSpells()[spellIndex];
		if(!s || pc->hasActed() || !pc->canSpendSpirit(s->cost)) return;
		
		enableTargetsForSpell(s);
	}
	
	void onNumberKey(int i){
		if(isNonInteractive() || selectedCharacter < 0) return;
		switch(i){
			/*case 0: {
				if(isActive(&attack_b)) onAttack();
				else if(isActive(&i_use_b)) onItemUse();
				else if(isActive(&i_equip_b)) onItemEquip();
				break;
			}
			case 1: {
				if(isActive(&move_b)) onMove();
				else if(isActive(&i_drop_b)) onItemDrop();
				else if(isActive(&i_take_b)) onItemTake();
				break;
			}
			case 2: {
				if(isActive(&spirit_b)) onSpirit();
				else if(isActive(&i_throw_b)) onItemThrow();
				break;
			}
			case 3: {
				if(isActive(&item_b)) onItem();
				else if(isActive(&i_give_b)) onItemGive();
				break;
			}
			case 4: {
				if(isActive(&look_b)) onLook();
				break;
			}
			case 5: {
				if(isActive(&status_b)) onStatus();
				else if(isActive(&i_ground_b)) onItemGround();
				break;
			}*/
			case 0:
			case 1:
			case 2: {
				selectSpell(i);
				break;
			}
		}
	}
	
	void onAffirmativeKey() {
		if (selectedCharacter < 0) return;

		if(paused) {
			onSpacebar();
			return;
		}
		
		if (selectedMenuItem >= 0 && isActive(dialogueButtons[selectedMenuItem])) {
			hideDialogueOptionButtons();
			cutscenes::startDialogueFromBranch(selectedMenuItem);
			selectedMenuItem = -1;
			return;
		}
		
		if (display::hasDialogue()) {
			cutscenes::advanceDialogue();
		} else if(item_mode != noItems) {
			if (selectedItem >= SELECT_GROUND) {
				onItemTake();
			}
		} else {
			// Default: look/interact
			PlayerCharacter* pc = characters::playable[selectedCharacter];
			int x = pc->getX();
			int y = pc->getY();
			switch(pc->getDirection()){
				case _up   : --y; break;
				case _down : ++y; break;
				case _left : --x; break;
				case _right: ++x; break;
			}
			bool looked = world::onLook(x,y);
			if (!looked) world::onLook(pc->getX(), pc->getY());
		}
	}
	
	void onArrowKey(direction d){
		if(display::isStartMenu()) return;
		if(paused) {
			switch(d) {
				case _up: {
					if (selectedMenuItem <= 0) selectedMenuItem = MAX_SETTINGS_MENU;
					else --selectedMenuItem;
					return;
				}
				case _down: {
					if (selectedMenuItem >= MAX_SETTINGS_MENU) selectedMenuItem = 0;
					else ++selectedMenuItem;
					return;
				}
				case _right: {
					switch(selectedMenuItem) {
						case 1:
							onAdjustMusic(1);
							return;
						case 2:
							onAdjustSound(1);
							return;
						case 3:
							onToggleFullscreen();
							return;
						// case 4: switch ui theme
					}
					return;
				}
				case _left: {
					switch(selectedMenuItem) {
						case 1:
							onAdjustMusic(-1);
							return;
						case 2:
							onAdjustSound(-1);
							return;
						case 3:
							onToggleFullscreen();
							return;
						// case 4: switch ui theme
					}
					return;
				}
			}
			return;
		}
		
		if(display::hasDialogue() && cutscenes::getResponseCount() > 0) {
			switch(d) {
				case _up:
				case _left:
					selectedMenuItem = (selectedMenuItem + cutscenes::getResponseCount() - 1) % cutscenes::getResponseCount();
					break;
				case _down:
				case _right:
					selectedMenuItem = (selectedMenuItem + 1) % cutscenes::getResponseCount();
					break;
			}
			return;
		}
		
		if(isNonInteractive()) return;
		
		if(weaponCommitButtonsShowing) {
			int maxCommit = characters::playable[selectedCharacter]->stepsLeft();
			if (maxCommit > 3) maxCommit = 3;
			switch(d) {
				case _left:
				case _down:
					if (selectedItem == -1) {
						selectedItem = 0;
					} else {
						selectedItem -= 1;
						if (selectedItem < 0) selectedItem = maxCommit;
					}
					return;
				case _right:
				case _up:
					if (selectedItem == -1) {
						selectedItem = maxCommit;
					} else {
						selectedItem += 1;
						if (selectedItem > maxCommit) selectedItem = 0;
					}
					return;
			}
		}
		
		if(item_mode == inventory) {
			switch(d) {
				case _right:
					if((selectedItem % SELECT_EQUIPMENT) % INVENTORY_ROW == INVENTORY_ROW-1) selectedItem -= INVENTORY_ROW;
					selectedItem += 1;
					break;
				case _left:
					if((selectedItem % SELECT_EQUIPMENT) % INVENTORY_ROW == 0) selectedItem += INVENTORY_ROW;
					selectedItem -= 1;
					break;
				case _down:
					if(selectedItem >= SELECT_WEAPON) selectedItem += SELECT_EQUIPMENT - SELECT_WEAPON;
					else if(selectedItem >= SELECT_EQUIPMENT) selectedItem -= SELECT_EQUIPMENT;
					else if(selectedItem + INVENTORY_ROW >= ITEMSPACE) selectedItem += SELECT_WEAPON + INVENTORY_ROW - ITEMSPACE;
					else selectedItem += INVENTORY_ROW;
					break;
				case _up:
					if(selectedItem >= SELECT_WEAPON) selectedItem = ITEMSPACE - INVENTORY_ROW + selectedItem - SELECT_WEAPON;
					else if(selectedItem >= SELECT_EQUIPMENT) selectedItem += SELECT_WEAPON - SELECT_EQUIPMENT;
					else if(selectedItem < INVENTORY_ROW) selectedItem += SELECT_EQUIPMENT;
					else selectedItem -= INVENTORY_ROW;
					break;				
			}
			
			if(selectedItem >= SELECT_WEAPON) selectWeapon(selectedItem - SELECT_WEAPON);
			else if(selectedItem >= SELECT_EQUIPMENT) selectEquipment(selectedItem - SELECT_EQUIPMENT);
			else selectItem(selectedItem);
			
			return;
		}
		
		PlayerCharacter* pc = characters::playable[selectedCharacter];
		pc->onArrowKey(d);
		onPlayerMove(pc);
		move_b.disable(pc->getHealth() > 0 && pc->stepsLeft() == 0);
	}
		
	void onSpacebar(){
		if(paused) {
			switch(selectedMenuItem) {
				case 0:
					onUnpause();
					return;
				case 3:
					onToggleFullscreen();
					return;
				// case 4: switch ui theme
			}
			return;
		}
		
		if(isNonInteractive() || !isActive(&attack_b)) {
			return;
		}
		PlayerCharacter* pc = characters::playable[selectedCharacter];
		if (weaponCommitButtonsShowing && selectedItem >= 0) {
			onCommitAttack(selectedItem);
		} else if(pc->getEquippedWeapon()->rtype == range_type::melee){
			int x = pc->getX();
			int y = pc->getY();
			switch(pc->getDirection()){
				case _up   : --y; break;
				case _down : ++y; break;
				case _left : --x; break;
				case _right: ++x; break;
			}
			targetX = x;
			targetY = y;
			
			showWeaponCommitButtons();
			
		} else onAttack();
	}
	
	void onItemHotkey(){
		if(isActive(&item_b)) onItem();
		else onCancel();
	}
	
	void onSpiritHotkey(){
		if(isActive(spellButtons[0])) onCancel();
		else if(isActive(&spirit_b)) onSpirit();
	}
	
	void onEndTurn(){
		hideTargets();
		hideInventory();

		for(int i=0; i<characters::playable.size(); ++i) {
			characters::playable[i]->skip(true);
		}
		
		if(world::playerTurnDone()) world::startEnemyTurn();
	}
	
	void onWait(){
		if(isNonInteractive() || selectedCharacter < 0) return;
		hideTargets();
		hideInventory();
		// Mark the current character as having moved and hide the action menu (go back to characters menu)
		//for(int i=0; i<characters::playable.size(); ++i) {
		//	characters::playable[i]->setActed(true);
		//	characters::playable[i]->setMoved(true);
		//}
		
		if(world::isCombat()) {
			characters::playable[selectedCharacter]->skip(true);
			toNextCharacter(false);
			if(world::playerTurnDone()) world::startEnemyTurn();
		} else {
			toNextCharacter(true);
		}
	}

	void onMove(){
		before_cancel_state = STATE_TARGET_MOVE;
		hideTargets();
		
		PlayerCharacter* pc = characters::playable[selectedCharacter];
		setTargetResult(targetMode::move);
		
		std::vector<std::pair<int,int> > moveTargets;
		if(pc->canMoveTo(pc->getX()-1, pc->getY())) moveTargets.push_back(std::make_pair(-1,0));
		if(pc->canMoveTo(pc->getX()+1, pc->getY())) moveTargets.push_back(std::make_pair(1,0));
		if(pc->canMoveTo(pc->getX(), pc->getY()-1)) moveTargets.push_back(std::make_pair(0,-1));
		if(pc->canMoveTo(pc->getX(), pc->getY()+1)) moveTargets.push_back(std::make_pair(0,1));
		allowTargets(moveTargets);
	}
	
	void onAttack(){
		hideTargets();
		setTargetResult(targetMode::attack);
		allowTargets(characters::playable[selectedCharacter]->getWeaponTargets());
	}
	
	void onCommitAttack(int strength) {
		setAnimationBlocking(true);
		characters::playable[selectedCharacter]->attackAction(targetX, targetY, strength);
	}
	
	void onSpirit(){
		hideTargets();
		hideInventory();
		selectedItem = -1;
		
		before_cancel_state = STATE_CHAR_MENU;
		setVisible(&menu_cancel_b, true);
		
		display::showSpellList();

		PlayerCharacter* pc = characters::playable[selectedCharacter];
		
		for(int i=0; i<SPELLSPACE; ++i) {
			setVisible(spellButtons[i], true);
			spellButtons[i]->disable( (!pc->getSpells()[i]) || pc->getSpells()[i]->cost > pc->getSpirit() );
		}
	}
	
	void onLook(){
		if (display::hasDialogue()) return;
		if (selectedCharacter < 0) return;

		PlayerCharacter* pc = characters::playable[selectedCharacter];
		if (pc->getHealth() <= 0 || pc->getRoomId() != world::getRoomId()) return;

		setTargetResult(targetMode::look);
		display::showLookInterface();
	}

	void showInventory(PlayerCharacter* pc) {
		for(int i=0; i<pc->getInventory().size(); ++i){
			setVisible(itemButtons[i], true);
			itemButtons[i]->disable(pc->getInventory()[i] == NULL);
		}
		for(int i=0; i<pc->getOffhandWeapons().size(); ++i){
			setVisible(weaponButtons[i], true);
			weaponButtons[i]->disable(pc->getOffhandWeapons()[i] == NULL);
		}
		setVisible(&activeWeaponButton, true);
		for(int i=0; i<EQUIPSPACE; ++i){
			setVisible(equipButtons[i], true);
			equipButtons[i]->disable(pc->getEquipment()[i] < 0);
		}
	}

	void onItem(){
		hideSpiritMenu();

		before_cancel_state = STATE_CHAR_MENU;
		selectedItem = 0;
		item_mode = itemMode::inventory;
		hideTurnMenu();
		setVisible(&menu_cancel_b, true);
		setVisible(&i_take_b, false);
		setVisible(&i_buy_b, false);
		display::showInventory();

		PlayerCharacter* pc = characters::playable[selectedCharacter];
		showInventory(pc);		
		int groundCount = world::countItemsAt(pc->getX(), pc->getY());
		if(groundCount) {
			if(groundScrollOffset > groundCount) groundScrollOffset = 0;
			refreshGroundItems();
		}

		selectItem(0);
	}

	void showShop() {
			before_cancel_state = STATE_CHAR_MENU;
			setVisible(&menu_cancel_b, true);

			item_mode = shop;

			selectedItem = -1;
			display::showShopItems();
			groundScrollOffset = 0;

			PlayerCharacter* pc = characters::playable[selectedCharacter];
			showInventory(pc);

			refreshShopItems();
	}
	
	void onStatus(){
		if(selectedCharacter < 0) return;
		hideTurnMenu();
		before_cancel_state = STATE_CHAR_MENU;
		display::showCharacterStatus();
		
		setVisible(&menu_cancel_b, true);
	}
	
	void onEnemyLook(Enemy* e){
		hideTurnMenu();
		before_cancel_state = STATE_CHAR_MENU;
		display::showEnemyStatus(e);
		
		setVisible(&menu_cancel_b, true);
	}
	
	void onItemUse(){
		std::cout << "item use" << std::endl;
		characters::playable[selectedCharacter]->useItem(selectedItem);
	}
	
	void onItemTake(){
		PlayerCharacter* pc = characters::playable[selectedCharacter];
		int x = pc->getX();
		int y = pc->getY();
		
		int itemIndex = selectedItem - SELECT_GROUND;
		
		const std::vector<ItemOrWeapon*>& pile = world::getItemsAt(x,y);
		switch(pile[itemIndex]->getPickupableType()){
			case ptWeapon: {
				weapon* w = static_cast<weapon*>(pile[itemIndex]);
				if(pc->takeWeapon(w)) world::removeNthItemAt(x,y,itemIndex);
				break;
			}
			case ptItem: {
				item* i = static_cast<item*>(pile[itemIndex]);
				if(pc->takeItem(i)) world::removeNthItemAt(x,y,itemIndex);
				break;
			}
			case ptCoin: {
				coinPile* c = static_cast<coinPile*>(pile[itemIndex]);
				characters::adjustCoins(c->getCoinType(), c->getCount());
				world::removeNthItemAt(x,y,itemIndex);
				break;
			}
		}
		
		if(item_mode == inventory) {
			onItem();
		}
		
		int remaining = world::countItemsAt(x,y);
		if(remaining == 0){
			hideGroundItems();
			
			if(item_mode == inventory) {
				selectItem(0);
			} else {
				showMainTurnMenu();
			}
		} else {
			itemIndex -= groundScrollOffset;
			if(itemIndex < 0) {
				incrementGroundScroll(itemIndex);
				itemIndex = 0;
			}
			if(itemIndex >= remaining) itemIndex = remaining - 1;
			selectGroundItem(itemIndex - groundScrollOffset);
			
			for(int i = remaining - groundScrollOffset; i < GROUNDSPACE; ++i) setVisible(groundItemButtons[i], false);		
		}
	}

	void onItemBuy(){
		PlayerCharacter* pc = characters::playable[selectedCharacter];
		int x = pc->getX();
		int y = pc->getY();
		
		int itemIndex = selectedItem - SELECT_GROUND;

		ItemOrWeapon* iow = world::getShopItem(itemIndex);
		switch(world::getShopItem(itemIndex)->getPickupableType()){
			case ptWeapon: {
				weapon* w = static_cast<weapon*>(iow);
				if(pc->takeWeapon(w)) {
					characters::adjustCoins(world::getShopCurrency(itemIndex), -world::getShopPrice(itemIndex));
					world::decrementShopSupply(itemIndex);
				}
				break;
			}
			case ptItem: {
				item* i = static_cast<item*>(iow);
				if(pc->takeItem(i)) {
					characters::adjustCoins(world::getShopCurrency(itemIndex), -world::getShopPrice(itemIndex));
					world::decrementShopSupply(itemIndex);
				}
				break;
			}
			case ptCoin: {
				coinPile* c = static_cast<coinPile*>(iow);
				characters::adjustCoins(c->getCoinType(), c->getCount());
				characters::adjustCoins((coinType)world::getShopCurrency(itemIndex), -world::getShopPrice(itemIndex));
				world::decrementShopSupply(itemIndex);
				break;
			}
		}

		showInventory(pc);

		int remaining = world::countShopItems();
		if(remaining == 0){
			hideGroundItems();
			item_mode = itemMode::inventory;
			selectItem(0);
		} else {
			itemIndex -= groundScrollOffset;
			if(itemIndex < 0) {
				incrementGroundScroll(itemIndex);
				itemIndex = 0;
			}
			if(itemIndex >= remaining) itemIndex = remaining - 1;
			selectGroundItem(itemIndex - groundScrollOffset);
			
			for(int i = remaining - groundScrollOffset; i < GROUNDSPACE; ++i) setVisible(groundItemButtons[i], false);		
		}
	}
	
	void onItemDrop(){
		PlayerCharacter* pc = characters::playable[selectedCharacter];
		if(selectedItem < SELECT_EQUIPMENT) {
			world::putItem(pc->getX(),pc->getY(),pc->popItem(selectedItem));
			itemButtons[selectedItem]->disable(true);
		} else if(selectedItem < SELECT_WEAPON){
			world::putItem(pc->getX(),pc->getY(),items::getItem(pc->getEquipment()[selectedItem-SELECT_EQUIPMENT]));
			pc->unequip(selectedItem - SELECT_EQUIPMENT);
			equipButtons[selectedItem-SELECT_EQUIPMENT]->disable(true);
		} else {
			world::putItem(pc->getX(),pc->getY(),pc->popWeapon(selectedItem-SELECT_WEAPON));
			weaponButtons[selectedItem-SELECT_WEAPON]->disable(true);
		}
		
		refreshGroundItems();
		
		hideItemActions();
	}
	
	void onItemThrow(){
		PlayerCharacter* pc = characters::playable[selectedCharacter];
		int range = pc->getRange();
		std::vector<std::pair<int, int> > targets;
		for(int i = -range; i <= range; ++i){
			for(int j = abs(i)-range; j <= range-abs(i); ++j){
				if((i || j) && world::isThrowTarget(pc->getInventory()[selectedItem]->id, i+pc->getX(), j+pc->getY())) targets.push_back(std::make_pair(i,j));
			}
		}
		hideInventory();
		target_mode = targetMode::throwItem;
		allowTargets(targets);
	}
	
	void onItemGive(){
		before_cancel_state = STATE_INVENTORY;
		hideInventory();
		setVisible(&menu_cancel_b, true);
		
		PlayerCharacter* pc = characters::playable[selectedCharacter];
		
		std::vector<std::pair<int, int> > targets;
		for(int i=0;i<characters::playable.size();++i){
			PlayerCharacter* otherPC = characters::playable[i];
			int deltaX = otherPC->getX() - pc->getX();
			int deltaY = otherPC->getY() - pc->getY();
			if(deltaX * deltaX + deltaY * deltaY == 1 && !otherPC->itemsFull()){ // TODO should check weapon space also
				targets.push_back(std::make_pair(deltaX, deltaY));
			}
		}
		if (selectedItem < SELECT_WEAPON) {
			const std::vector<std::shared_ptr<NPC>> npcs = world::getNPCs();
			for(int i=0;i<npcs.size();++i){
				int deltaX = npcs[i]->getX() - pc->getX();
				int deltaY = npcs[i]->getY() - pc->getY();
				if(deltaX * deltaX + deltaY * deltaY == 1){
					targets.push_back(std::make_pair(deltaX, deltaY));
				}
			}
		}
		target_mode = targetMode::giveItem;
		allowTargets(targets);
	}
	
	void incrementGroundScroll(int delta) {
		groundScrollOffset += delta;
		if(groundScrollOffset < 0) groundScrollOffset = 0;
		
		PlayerCharacter* pc = characters::playable[selectedCharacter];
		int itemCount = world::countItemsAt(pc->getX(), pc->getY());
		
		if(groundScrollOffset >= itemCount) groundScrollOffset = itemCount;
		
		groundScrollUp.disable(groundScrollOffset == 0);
		groundScrollDown.disable(groundScrollOffset + GROUNDSPACE == itemCount);
		
		if(selectedItem >= SELECT_GROUND) {
			selectedItem -= delta;
			if(selectedItem < SELECT_GROUND) selectedItem = SELECT_GROUND;
			if(selectedItem >= GROUNDSPACE) selectedItem = GROUNDSPACE-1;
		}
	}
	
	void onItemEquip(){
		if(selectedItem >= SELECT_WEAPON) {
			characters::playable[selectedCharacter]->equipWeapon(selectedItem-SELECT_WEAPON);
		} else {
			characters::playable[selectedCharacter]->useItem(selectedItem, true);
			hideInventory();
			onItem();
		}
		hideItemActions();
		selectedItem = SELECT_EQUIPMENT;
		
	}
	
	void onItemRemove(){
		PlayerCharacter* pc = characters::playable[selectedCharacter];
		pc->takeItem(items::getItem(pc->getEquipment()[selectedItem-SELECT_EQUIPMENT]));
		pc->unequip(selectedItem-SELECT_EQUIPMENT);
		hideItemActions();
		equipButtons[selectedItem-SELECT_EQUIPMENT]->disable(true);
		onItem();
		
	}
	
	void onCancel(){
		hideTargets();
		if(!isActive(&menu_cancel_b)) return;
		//std::cout << "return to state " << before_cancel_state << std::endl;
		switch(before_cancel_state){
			case STATE_TARGET_MOVE:
				showMainTurnMenu();
				if(characters::playable[selectedCharacter]->stepsLeft() > 0){
					hideInventory();
					onMove();
				}
				break;
			case STATE_INVENTORY:
				onItem();
				break;
			default:
				item_mode = noItems;
				showMainTurnMenu();
		}
	}
	
	void onActionComplete() {
		if(!world::isPlayerTurn()) return;
		setAnimationBlocking(false);
		if(display::hasDialogue()) return;
		if(world::playerTurnDone()) {
			world::startEnemyTurn();
		} else {
			if(characters::playable[selectedCharacter]->isDone()) toNextCharacter(false);
			showMainTurnMenu();
		}
	}
	
	void onTravelModeButton(bool tm) {
		manualTravelMode = tm;
		setTravelMode(tm);
	}
	
	void setTravelMode(bool tm){

		if(tm) {
			for(int i=0; i<characters::playable.size(); ++i) if (i != selectedCharacter) {
				characters::playable[i]->setXY(-1, -1);
			}
		} else {
			PlayerCharacter* pc = characters::playable[selectedCharacter];
			for(int i=0; i<characters::playable.size(); ++i) {
				characters::playable[i]->setXY(pc->getX(), pc->getY());
			}
		}

		travelMode = tm;
		if (characters::playable.size() > 1) {
			setVisible(&travel_on_b, !tm);
			setVisible(&travel_off_b, tm);
		}
	}
	
	void restoreDefaultTravelMode() {
		setTravelMode(manualTravelMode);
	}
	
	bool isTravelMode(){
		return travelMode;
	}
	
	void hideTurnMenu(){
		hideTargets();
		setVisible(&move_b, false);
		setVisible(&attack_b, false);
		setVisible(&spirit_b, false);
		setVisible(&item_b, false);
		setVisible(&status_b, false);
		setVisible(&look_b, false);
		setVisible(&return_b, false);
		setVisible(&menu_cancel_b, false);
		setVisible(&wait_b, false);
	}
	
	void hideTargets(){
		for(int i=0; i<targetButtons.size(); ++i){
			for(int j=0; j<targetButtons[i].size(); ++j){
				setVisible(targetButtons[i][j], false);
			}
		}
		for(auto itr=largeTargetButtons.begin(); itr != largeTargetButtons.end(); ++itr){
			setVisible(itr->second, false);
		}
		for(int i=0; i<weaponCommitButtons.size(); ++i) {
			setVisible(weaponCommitButtons[i], false);
		}
		weaponCommitButtonsShowing = false;
	}
	
	void hideSpiritMenu(){
		for(int i=0; i<spellButtons.size(); ++i){
			setVisible(spellButtons[i], false);
		}
	}
	
	void hideGroundItems(){
		setVisible(&groundScrollUp, false);
		setVisible(&groundScrollDown, false);
		
		for(int i=0; i<groundItemButtons.size(); ++i){
			setVisible(groundItemButtons[i], false);
		}
		
		if(item_mode == ground) {
			item_mode = noItems;
			display::clearView();
		}
	}
	
	void hideInventory(){
		for(int i=0; i<itemButtons.size(); ++i){
			setVisible(itemButtons[i], false);
		}
		for(int i=0; i<weaponButtons.size(); ++i){
			setVisible(weaponButtons[i], false);
		}
		setVisible(&activeWeaponButton, false);
		for(int i=0; i<equipButtons.size(); ++i){
			setVisible(equipButtons[i], false);
		}
		
		hideGroundItems();
		
		item_mode = noItems;
		hideSpiritMenu();
		hideItemActions();
		display::clearView();
	}

	void resetCharactersMenu() {
		hideTurnMenu();
		
		for(int i=0; i<pcButtons.size(); ++i){
			pcButtons[i]->disable(characters::playable[i]->getHealth() <= 0);
			setVisible(pcButtons[i], true);
		}
		
		if(!travelMode || world::isCombat()) {
			selectedCharacter = characters::playable.size() - 1;
			toNextCharacter(false);
		}
	}
	
	void setVisible(Button* b, bool visible){
		if(visible) gameVisibleButtons.insert(b);
		else {
			b->reset();
			gameVisibleButtons.erase(b);
		}
	}
	
	void setMenuVisible(Button* b, bool visible){
		if(visible) menuVisibleButtons.insert(b);
		else {
			b->reset();
			menuVisibleButtons.erase(b);
		}
	}
	
	void setAnimationBlocking(bool blocking) {
		animationBlocking = blocking;
		if(blocking) {
			hideInventory();
			hideTurnMenu();
			setVisible(&wait_b, false);
		}
	}
	
	void hideDialogueOptionButtons() {
		for(int i=0; i<dialogueButtons.size(); ++i) {
			setVisible(dialogueButtons[i], false);
		}
	}
	
	void showDialogueOptionButtons(int count) {
		selectedMenuItem = -1;
		for(int i=0; i<count; ++i) {
			setVisible(dialogueButtons[i], true);
		}
	}
	
	void allowTargets(const std::vector<std::pair<int,int> >& locations){
		for(int i=0; i<locations.size(); ++i){
			setVisible(targetButtons[locations[i].first+MAX_RANGE][locations[i].second+MAX_RANGE], true);
		}
		// hide large targets
		for(auto itr=largeTargetButtons.begin(); itr != largeTargetButtons.end(); ++itr){
			setVisible(itr->second, false);
		}
	}
	
	void allowLargeTarget(int radius) {
		hideTargets(); // hide all targets besides the desired large target
		if( largeTargetButtons.find(radius) == largeTargetButtons.end() ) {
			// construct a Large Target...
			Button* b = new Button(
				radius,
				(SCREENWIDTH - TILESIZE)/2 - radius*TILESIZE,
				(SCREENHEIGHT - TILESIZE)/2 - radius*TILESIZE
			);
			b->effect = [](){buttons::target(0, 0);};
			largeTargetButtons[radius] = b;
		}
		setVisible(largeTargetButtons[radius], true);
	}
	
	void setCombat(bool combat) {
		if (combat) {
			// Temporarily set travel mode to true. But on the next room load, it'll go to whatever it was before.
			setTravelMode(false);
		}
		travel_on_b.disable(combat);
		travel_off_b.disable(combat);
	}
	
	const std::unordered_set<Button*>& listButtons(){
		if (paused || display::isStartMenu()) return menuVisibleButtons;
		else return gameVisibleButtons;
	}
	
	void showDeathScreen() {
		for(int i=0; i<pcButtons.size(); ++i){
			pcButtons[i]->disable(true);
		}
		setVisible(&respawn_b, true);
		display::showDeathScreen();
	}
	
	void onReturn() {
		PlayerCharacter* pc = characters::playable[selectedCharacter];
		pc -> setRoomId(world::getRoomId());
		pc -> setMoved(world::isCombat());
		pc -> setActed(true);
		
		int x = pc->getX();
		int y = pc->getY();
		
		if(x<0) x = 0;
		if(y<0) y = 0;
		if(x>=world::width) x = world::width-1;
		if(y>=world::height) y = world::height-1;
		
		pc->setXY(x, y);
	}
	
	void onRespawn() {
		setVisible(&respawn_b, false);
		for(int i=0; i<pcButtons.size(); ++i){
			pcButtons[i]->disable(false);
		}
		world::onRespawn();
	}
}

