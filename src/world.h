#ifndef __WORLD__
#define __WORLD__

#include <map>
#include <vector>
#include <memory>
#include <unordered_set>

#include "core.h"
#include "terrain.h"
#include "items.h"
#include "savefile.h"

#define CRATE_TILE 0xfe
#define FLOOR_BREAK_TILE 0xfd

class StattedCharacter;
class PlayerCharacter;
class Enemy;
class NPC;

namespace world {
	void loadNew(int);
	
	void loadMap(int mapid);
	void setTileImage(int x, int y, int t);
	int getTileImage(int x, int y);
	
	unsigned char** getAllTileImages();
	
	int getRoomId();
	
	//extern std::map<std::pair<int,int>,std::vector<ItemOrWeapon*>*> itemPiles;
	const std::vector<Enemy*>& getEnemies();
	const std::vector<std::shared_ptr<NPC>>& getNPCs();
	
	Enemy* getEnemyAt(int,int);
	std::shared_ptr<NPC> getNPCAt(int,int);
	StattedCharacter* getCharacterAt(int,int);

	bool hasLookText(int,int);
	bool hasCheckpoint(int,int);

	int countItemsAt(int,int);
	const std::vector<ItemOrWeapon*>& getItemsAt(int,int);
	void removeNthItemAt(int,int,int);
	void putItem(int, int, ItemOrWeapon*);
	
	void damageAtTile(int, int, int, damage_type);
	void damageTerrain(int, int, int, damage_type);
	void onWaterHit(int, int);
	
	void processDoor(PlayerCharacter*, int, int);
	
	int itemPileSpriteIndex(int x, int y);
	
	int getDoorIndex(int, int);
	
	bool outOfBounds(int, int);
	
	bool isThrowTarget(int itemIndex, int x, int y);

	void handleTransientTerrain(unsigned char, StattedCharacter*);	
	void handleCustomTerrain(int, StattedCharacter*);

	int collectSpellItem(int, int);

	void handleCheckpoint(int);
	void checkPlayerDeath();
	
	extern unsigned char** types;
	
	extern int width;
	extern int height;
	
	void addNpc(int, int, int, int);
	void removeNpc(int);
	void npcWalk(int, direction, int);
	
	bool onLook(int, int);
	
	Enemy* putEnemy(int, int, int, ItemOrWeapon*);
	void onEnemyDeath();
	
	bool playerTurnDone();
	void startEnemyTurn();
	void checkPlayerTurn();
	
	void onRespawn();
	
	bool hasRoomFlag(int);
	void setRoomFlags(int);
	
	bool hasGlobalFlag(int);
	void setGlobalFlag(int);
	void removeGlobalFlag(int);
	const std::unordered_set<int>& getGlobalFlags();

	int countShopItems();
	coinType getShopCurrency(int);
	int getShopPrice(int);
	ItemOrWeapon* getShopItem(int);
	int getShopQuantity(int);
	void decrementShopSupply(int);

	int getDungeonId();

	void enableCombat(bool);
	bool isCombat();
	bool isPlayerTurn();
	
	void clearRoomCache();
	
	void cleanup();
}

#endif
