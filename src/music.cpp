#include <iostream>
#include <unordered_map>

#include "music.h"
#include "characters.h"

#include <SFML/Audio.hpp>

#define BOSS_MUSIC 3 // Temporary workaround

namespace buttons {
	int getSelectedCharacter();
}

const int maxParty = 3; // declaring as a constant so as to not interfere with #defines

namespace music {
	
	sf::Music mainMusic;
	
	int currentMusic = -1;
	int currentSubMusic = -1;

	bool bossInit = false;
	sf::Music bossMusicOverlays[maxParty];

	int musicVolume = MAX_VOLUME/2;
	int soundVolume = MAX_VOLUME;

	void switchTo(int musicId) {

		if(currentMusic == musicId) return;
		if(currentMusic == BOSS_MUSIC) {
			for (int i=0; i<characters::playable.size(); ++i) {
				bossMusicOverlays[i].stop();
			}
		}

		currentMusic = musicId;

		mainMusic.stop();
		if(currentMusic < 0) return;

		if(!mainMusic.openFromFile("mus/"+std::to_string(musicId)+".ogg")) {
			std::cerr << "Cannot load music file!" << std::endl;
		} else {
			if(musicId == BOSS_MUSIC) {
				for (int i=0; i<characters::playable.size(); ++i) {
					bossMusicOverlays[i].openFromFile("mus/"+std::to_string(musicId)+"-"+std::to_string(i)+".ogg");
					bossMusicOverlays[i].setLoop(true);
					bossMusicOverlays[i].setVolume(musicVolume * 30 / MAX_VOLUME);
				}
				bossInit = true;
				currentSubMusic = buttons::getSelectedCharacter();
				bossMusicOverlays[currentSubMusic].setVolume(musicVolume * 100 / MAX_VOLUME);
				for (int i=0; i<characters::playable.size(); ++i) {
					//bossMusicOverlays[i].setPlayingOffset(mainMusic.getPlayingOffset()); // Just in case of desync during loading
					bossMusicOverlays[i].play();
				}
			}
			mainMusic.play();
			mainMusic.setLoop(true);
		}
	}
	
	std::unordered_map<std::string, sf::Sound*> soundCache;

	void cleanup() {
		mainMusic.stop();
		
		// TODO iterate over sound cache here and delete buffers
	}

	void onCharacterSelect() {
		if (currentMusic == BOSS_MUSIC) {
			int newCharacter = buttons::getSelectedCharacter();
			//if (currentSubMusic >= 0) bossMusicOverlays[currentSubMusic].setVolume(0);
			currentSubMusic = newCharacter;
			
			std::cout << "Main offset: " << mainMusic.getPlayingOffset().asMicroseconds() << std::endl;
			for (int i=0; i<characters::playable.size(); ++i) {
				bossMusicOverlays[i].setVolume(musicVolume * 30 / MAX_VOLUME);
				std::cout << "Volume: " << bossMusicOverlays[i].getVolume() << "; Offset: " << bossMusicOverlays[i].getPlayingOffset().asMicroseconds() << std::endl;
			}
			bossMusicOverlays[currentSubMusic].setVolume(musicVolume * 100 / MAX_VOLUME);
//			bossMusicOverlays[currentSubMusic].setPlayingOffset(mainMusic.getPlayingOffset()); // this was apparently causing more harm than good
		}
	}

	void cacheSound(std::string soundName) {
		if(soundCache.find(soundName) == soundCache.end()) {
			sf::SoundBuffer* buffer = new sf::SoundBuffer;
			buffer->loadFromFile("snd/"+soundName+".wav");
			sf::Sound* sound = new sf::Sound(*buffer);
			soundCache[soundName] = sound;
		}
	}
	
	void playSound(std::string soundName, double pitch) {
		cacheSound(soundName);
		
		sf::Sound* sound = soundCache[soundName];
		sound->setPitch(pitch);
		sound->setVolume(soundVolume * 100 / MAX_VOLUME);
		sound->play();
	}
	
	void setMusicVolume(int v) {
		if (v<0 || v>MAX_VOLUME) return;
		musicVolume = v;
		mainMusic.setVolume(v * 100 / MAX_VOLUME);
		
		if (currentMusic == BOSS_MUSIC) {
			bossMusicOverlays[currentSubMusic].setVolume(musicVolume * 100 / MAX_VOLUME);
		}
	}
	
	void setSoundVolume(int v) {
		if (v<0 || v>MAX_VOLUME) return;
		soundVolume = v;
	}
	
	int getMusicVolume() {
		return musicVolume;
	}
	
	int getSoundVolume() {
		return soundVolume;
	}
}
