#include <iostream>
#include <fstream>
#include <sstream>
#include <unordered_set>

#ifdef cpp17fs
	#include <filesystem>
#endif

#include "savefile.h"
#include "characters.h"

// My stupid compiler won't let me #include <filesystem> and I can't seem to upgrade to a version that does.
// So, until then, you get three save slots.

namespace world {
	void loadMap(int);
	void saveRooms();
	int getRoomId();
	void addGlobalFlag(int);
	void setFileId(int);
	const std::unordered_set<int>& getGlobalFlags();
}

namespace buttons {
	void selectCharacter(int);
}

namespace savefile {
	
	void load(int fileId) {
		std::ifstream global("save/" + std::to_string(fileId) + "/global");
		std::string line;
		std::stringstream lineStream;

		getline(global, line);
		lineStream.str(line);
		lineStream.clear();

		int mapid;
		int partySize = 1;
		if(global.good()) {
			lineStream >> mapid >> partySize;

			// event flags
			getline(global, line);
			lineStream.str(line);
			lineStream.clear();

			int temp;
			while(!lineStream.eof()) {
				lineStream >> temp;
				world::addGlobalFlag(temp);
			}

			// coins
			getline(global, line);
			lineStream.str(line);
			lineStream.clear();

			for (int i=0; i < COIN_TYPE_COUNT; ++i) {
				lineStream >> temp;
				characters::setCoins((coinType)i, temp);
			}
		} else {
			mapid = 0;
			for (int i=0; i < COIN_TYPE_COUNT; ++i) {
				characters::setCoins((coinType)i, 0);
			}
		}
		characters::loadSave(fileId, partySize);
		for(int i=0; i<partySize; ++i) characters::playable[i]->setRoomId(mapid);
		buttons::selectCharacter(0);
		world::setFileId(fileId);
		world::loadMap(mapid);
	}
	
	
	void save(int fileId) {
		std::cout << "Saving..." << std::endl;
		for(int i=0; i<characters::playable.size(); ++i) {
			characters::playable[i]->save(fileId);
		}
		
		world::saveRooms();

		std::ofstream global("save/"+std::to_string(fileId)+"/global");

		global << world::getRoomId() << " ";
		global << characters::playable.size();

		global << std::endl;

		// TODO problem with trailing spaces?
		for(int flag : world::getGlobalFlags()) global << flag << " ";

		global << std::endl;

		for (int i=0; i < COIN_TYPE_COUNT; ++i) {
			if (i > 0) global << " ";
			global << characters::getCoinCount((coinType)i);
		}
		global << std::endl;

		std::cout << "Saved" << std::endl;
	}
	
	bool exists(int fileId) {
#ifdef cpp17fs
		return std::filesystem::exists("save/"+std::to_string(fileId));
#else
		// stupid hack for bad compiler
		std::ifstream global("save/" + std::to_string(fileId) + "/global");
		return global.good();
#endif
	}
	
	void clear(int fileId) {
#ifdef cpp17fs
// curse this stupid compiler
		std::filesystem::remove_all("save/"+std::to_string(fileId));
		
		std::filesystem::create_directories("save/"+std::to_string(fileId)+"/char");
		std::filesystem::create_directories("save/"+std::to_string(fileId)+"/data");
		std::filesystem::create_directories("save/"+std::to_string(fileId)+"/status");
		std::filesystem::create_directories("save/"+std::to_string(fileId)+"/tiles");
#endif
	}
}
